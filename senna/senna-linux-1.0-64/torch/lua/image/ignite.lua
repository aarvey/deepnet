
local torchdir = os.getenv('HOME') .. '/torch/'
dofile(torchdir .. 'scripts/lighter.lua')

ignite.torchPackage("image",
                    "-I" .. torchdir .. "packages/torch " 
		    ..  "-I" .. ignite.pwd(),
                     "-L" .. torchdir .. "packages/torch/obj/"
			.. config.name .. " "
		 	 .. " -ltorch ",
                    {torchdir .. "packages/torch" })
