dofile('../../scripts/lighter.lua')

ignite.torchPackage("nn",
                    "-I../torch -I../ml -I../random -I" .. ignite.pwd(),
                    "-L../torch/obj/" .. config.name .. " -L../ml/obj/" .. config.name .. " -L../random/obj/" .. config.name .. " -ltorch -lrandom -lml",
                    {"../torch", "../ml", "../random"})
