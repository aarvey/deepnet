local _StochasticGradient_ = torch.newluametatable('nn.StochasticGradient')

local function StochasticGradient(module, criterion)
   self = {}
   self.learningRate = 0.01
   self.learningRateDecay = 0
   self.endAccuracy = 0.0001
   self.maxIteration = 25
   self.shuffleIndices = true
   self.module = module
   self.criterion = criterion
   self.measurers = {}
   setmetatable(self, _StochasticGradient_)
   return self
end
rawset(nn, 'StochasticGradient', StochasticGradient)

function _StochasticGradient_:addMeasurer(measurer, dataset)
   if not self.measurers[dataset] then
      self.measurers[dataset] = {}
   end
   table.insert(self.measurers[dataset], measurer)
end

function _StochasticGradient_:train(dataset)
   local iteration = 0
   local currentError = 0;
   local previousError = math.huge
   local currentLearningRate = self.learningRate;
   local module = self.module
   local criterion = self.criterion

   local shuffledIndices = random.shuffledIndices(dataset:size())
   if not self.shuffleIndices then
      for t = 1,dataset:size() do
         shuffledIndices[t] = t;
      end
   end

   print("# StochasticGradient: training");

   for dataset,_ in pairs(self.measurers) do
      for _,measurer in pairs(self.measurers[dataset]) do
         measurer:reset()
      end
   end

   while true do
      currentError = 0;
      for t = 1,dataset:size() do
         local example = dataset[shuffledIndices[t]]
         local input = example[1]
         local target = example[2]

         currentError = currentError + criterion:forward(module:forward(input), example);
         module:zeroGradParameters()
         module:backward(criterion:backward(module.output, example), input);
         module:updateParameters(currentLearningRate)

         if self.measurers[dataset] then
            for _,measurer in pairs(self.measurers[dataset]) do
               measurer:measureExample(example)
            end
         end
      end

      for testDataset,_ in pairs(self.measurers) do
         if testDataset ~= dataset then
            for t=1,testDataset:size() do
               local example = testDataset[t]
               local input = example[1]
               module:forward(input)
               for _,measurer in pairs(self.measurers[testDataset]) do
                  measurer:measureExample(example)
               end
            end
         end
         for _,measurer in pairs(self.measurers[testDataset]) do
            measurer:measureIteration(iteration)
         end
      end
      
      currentError = currentError / dataset:size()
      print("# current error = " .. currentError)
      if math.abs(previousError - currentError) < self.endAccuracy then
         break
      end
      previousError = currentError
      iteration = iteration + 1
      currentLearningRate = self.learningRate/(1+iteration*self.learningRateDecay);
      if self.maxIteration > 0 and iteration >= self.maxIteration then
         print("# StochasticGradient: you have reached the maximum number of iterations")
         break
      end
   end
end

function _StochasticGradient_:write(archiver)
   archiver:writeDouble(self.learningRate)
   archiver:writeDouble(self.learningRateDecay)
   archiver:writeDouble(self.endAccuracy)
   archiver:writeInt(self.maxIteration)
   archiver:writeBool(self.shuffleIndices)
   archiver:writeObject(self.module)
   archiver:writeObject(self.criterion)
end

function _StochasticGradient_:read(archiver)
   self.learningRate = archiver:readDouble()
   self.learningRateDecay = archiver:readDouble()
   self.endAccuracy = archiver:readDouble()
   self.maxIteration = archiver:readInt()
   self.shuffleIndices = archiver:readBool()
   self.module = archiver:readObject()
   self.criterion = archiver:readObject()
   self.measurers = {}
end
