local CPPrepmat = lab.repmat
local function repmat(...)
   local n = select('#', ...)
   local dimension
   if n == 2 and torch.type(select(2, ...)) == 'torch.IntStorage' then
      dimension = select(2, ...)
   else
      dimension = torch.IntStorage(n-1)
      for i=2,n do
         dimension[i-1] = select(i, ...)
      end
   end
   return CPPrepmat(select(1, ...), dimension)
end
rawset(lab, 'repmat', repmat)

local CPPreshape = lab.reshape
local function reshape(...)
   local n = select('#', ...)
   local dimension
   if n == 2 and torch.type(select(2, ...)) == 'torch.IntStorage' then
      dimension = select(2, ...)
   else
      dimension = torch.IntStorage(n-1)
      for i=2,n do
         dimension[i-1] = select(i, ...)
      end
   end
   return CPPreshape(select(1, ...), dimension)
end
rawset(lab, 'reshape', reshape)
