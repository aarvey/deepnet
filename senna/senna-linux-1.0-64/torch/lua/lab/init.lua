require('torch')
require('random')
require('liblab')

torch.include('lab', 'matelem.lua')
torch.include('lab', 'matmanip.lua')
