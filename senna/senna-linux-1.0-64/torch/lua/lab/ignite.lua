local torchdir = os.getenv('HOME') .. '/torch/'

dofile(torchdir .. 'scripts/lighter.lua')

ignite.torchPackage("lab",
                    "-I" .. ignite.pwd() .. " -I" .. torchdir .. "packages/torch -I" .. torchdir .. "packages/random",
                    "-L" .. torchdir .. "packages/torch/obj/" .. config.name .. " -L" .. torchdir .. "packages/random/obj/" .. config.name .. " -ltorch -lrandom",
                    {torchdir .. "packages/torch", torchdir .. "packages/random"})
