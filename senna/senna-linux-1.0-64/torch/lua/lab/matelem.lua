local function ones(...)
   local n = select('#', ...)
   local dimension
   if n == 1 and torch.type(select(1, ...)) == 'torch.IntStorage' then
      dimension = select(1, ...)
   else
      dimension = torch.IntStorage(n)
      for i=1,n do
         dimension[i] = select(i, ...)
      end
   end
   local tensor = torch.Tensor(dimension)
   tensor:fill(1)
   return tensor
end
rawset(lab, 'ones', ones)

local CPPrand = lab.rand
local function rand(...)
   local n = select('#', ...)
   local dimension
   if n == 1 and torch.type(select(1, ...)) == 'torch.IntStorage' then
      dimension = select(1, ...)
   else
      dimension = torch.IntStorage(n)
      for i=1,n do
         dimension[i] = select(i, ...)
      end
   end
   return CPPrand(dimension)
end
rawset(lab, 'rand', rand)

local CPPrandn = lab.randn
local function randn(...)
   local n = select('#', ...)
   local dimension
   if n == 1 and torch.type(select(1, ...)) == 'torch.IntStorage' then
      dimension = select(1, ...)
   else
      dimension = torch.IntStorage(n)
      for i=1,n do
         dimension[i] = select(i, ...)
      end
   end
   return CPPrandn(dimension)
end
rawset(lab, 'randn', randn)

local function zeros(...)
   local n = select('#', ...)
   local dimension
   if n == 1 and torch.type(select(1, ...)) == 'torch.IntStorage' then
      dimension = select(1, ...)
   else
      dimension = torch.IntStorage(n)
      for i=1,n do
         dimension[i] = select(i, ...)
      end
   end
   local tensor = torch.Tensor(dimension)
   tensor:zero()
   return tensor
end
rawset(lab, 'zeros', zeros)
