-- some tricks to handle Lua Torch Classes...

local CPPArchiver = torch.newArchiver__internal__

local function Archiver(...)
   local self = CPPArchiver(...)
   if self then
      torch.setenv(self, {})
   end
   return self
end
rawset(torch, 'Archiver', Archiver)
rawset(torch, 'newArchiver__internal__', nil)

local function writeObject(self, object)
   if not object then
      self:writeInt(-1)
      return
   end

   local newObject
   local index
   if torch.luametatable(object:className()) then
      index = torch.getenv(self)[object]
      if not index then
         self._objects:add(torch.Object())
         index = self._objects:size()-1
         torch.getenv(self)[object] = index
         newObject = true
      end
   else
      index = self._objects:contains(object)-1
      if index < 0 then
         self._objects:add(object)
         index = self._objects:size()-1
         newObject = true
      end
   end
   
   self:writeInt(index)

   if newObject then
      local className = object:className()
      local classNameStorage = torch.CharStorage(#className)
      for i=1,#className do
         classNameStorage[i] = string.sub(className, i, i)
      end
      self:writeInt(#className)
      self:writeChar(classNameStorage)
      object:write(self)
   end
end
rawset(torch.metatable('torch.Archiver')['.fn'], 'writeObject', writeObject)

local function readObject(self)
   local index = self:readInt()
   if index < 0 then
      return nil
   end

   if torch.getenv(self)[index] then
      return torch.getenv(self)[index]
   end

   if index < self._objects:size() then
      return self._objects[index+1]
   end

   local size = self:readInt()
   local classNameStorage = self:readChar(size)
   local className = ""
   for i=1,classNameStorage:size() do
      className = className .. classNameStorage[i]
   end
   local object
   if torch.luametatable(className) then
      self._objects[index+1] = torch.Object()
      object = {}
      setmetatable(object, torch.luametatable(className))
      torch.getenv(self)[index] = object
      object:read(self)
   else
      object = torch.Factory_create(className)
      self._objects[index+1] = object
      object:read(self)
   end
   return object
end
rawset(torch.metatable('torch.Archiver')['.fn'], 'readObject', readObject)
