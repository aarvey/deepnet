require('libtorch')

--- package stuff
local function packageLuaPath(name)
   if not name then
       ret = string.match(packageLuaPath('torch'), '(.*)/')
       if not ret then --windows?
           ret = string.match(packageLuaPath('torch'), '(.*)\\')
       end
       return ret 
   end
   for path in string.gmatch(package.path, "(.-);") do
      path = string.gsub(path, "%?", name)
      local f = io.open(path)
      if f then
         f:close()
         ret = string.match(path, "(.*)/")
         if not ret then --windows?
             ret = string.match(path, "(.*)\\")
         end
         return ret
      end
   end
end
rawset(torch, 'packageLuaPath', packageLuaPath)
 
local function include(package, file)
   dofile(torch.packageLuaPath(package) .. '/' .. file) 
end
rawset(torch, 'include', include)

-- metatable stuff
local luametatables = {}

local function newluametatable(className)
   local metatable = {}
   luametatables[className] = metatable
   metatable.className = function()
                            return className
                         end
   metatable.__index = metatable
   metatable.__tostring = metatable.className
   metatable.write = function()
                     end
   metatable.read = function()
                    end
   return metatable
end
rawset(torch, 'newluametatable', newluametatable)

local function luametatable(className)
   return luametatables[className]
end
rawset(torch, 'luametatable', luametatable)

-- includes
rawset(torch, 'DiskFile', torch.newDiskFile__internal__)
rawset(torch, 'newDiskFile__internal__', nil)
torch.include('torch', 'Tensor.lua')
torch.include('torch', 'Archiver.lua')
torch.include('torch', 'help.lua')
