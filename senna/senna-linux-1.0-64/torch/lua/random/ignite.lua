dofile('../../scripts/lighter.lua')

ignite.torchPackage("random",
                    "-I../torch -I" .. ignite.pwd(), 
                    "-L../torch/obj/" .. config.name .. " -ltorch",
                    {"../torch"})
