require('torch')
require('liblcairo')

--[[
function packageLuaPath(name)
   for path in string.gmatch(package.path..';', "(.-);") do
      path = string.gsub(path, "%?", name)
      local f = io.open(path)
      if f then
         f:close()
         return string.match(path, ".*/")
      end
   end
end

local previous = gfx
dofile(packageLuaPath('gfx') .. '/libgfx.lua')
rawset(torch, 'gfx', gfx)
gfx = previous
]]--
