dofile('../../scripts/lighter.lua')

-----------

--if your system doesn't support pkg-config to find cairo flags and libraries
--then override cairo_cflags in your main opt 


if config.cairo_cflags==nil then config.cairo_cflags="" end
if config.cairo_dflags==nil then config.cairo_dflags = "`pkg-config cairo --cflags` -DCAIRO_HAS_PDF_SURFACE=1 -DCAIRO_HAS_PS_SURFACE=1" end
if config.cairo_lib==nil    then config.cairo_lib = "`pkg-config cairo --libs`" end

config.cf   = config.cf.." "..config.cairo_cflags.." "..config.cairo_dflags
config.cppf = config.cf
config.swgf = config.swgf.." "..config.cairo_dflags
config.lib  = config.lib.." "..config.cairo_lib

------------

config.swig = string.gsub(config.swig, '-factory', '')
ignite.torchPackage("lcairo",
                    "-I../torch -I" .. ignite.pwd() .. " -D_TORCH=1",
                    "-L../torch/obj/" .. config.name .. " -ltorch -lpthread",
                    {"../torch"})

--[[
--remove old version in wrong place..
os.execute("rm -f " .. config.install ..  "/lib/libgfx.lua")

os.execute("mkdir -p " .. config.install .. "/gfx/")
os.execute("cp init.lua " .. config.install .. "/gfx/")
os.execute("cp libgfx.lua " .. config.install ..  "/gfx/")
]]--
