dofile('../../scripts/lighter.lua')

ignite.torchPackage("ml",
                    "-I../torch -I" .. ignite.pwd(), 
                    "-L../torch/obj/" .. config.name .. " -ltorch",
                    {"../torch"})
