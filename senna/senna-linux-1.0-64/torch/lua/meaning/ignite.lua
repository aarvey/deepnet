local torchdir = os.getenv('HOME') .. '/torch/'

dofile(torchdir .. 'scripts/lighter.lua')

ignite.torchPackage("meaning",
                    "-I" .. torchdir .. "packages/torch -I" .. torchdir .. "packages/ml -I" .. torchdir .. "packages/nn -I" .. torchdir .. "packages/random -I" .. ignite.pwd(),
                    "-L" .. torchdir .. "packages/torch/obj/" .. config.name .. " -L" .. torchdir .."packages/ml/obj/" .. config.name .. " -L" .. torchdir .. "packages/nn/obj/" .. config.name .. " -L" .. torchdir .. "packages/random/obj/" .. config.name .. " -lnn -lrandom -lml -ltorch",
                    {torchdir .. "packages/torch", torchdir .. "packages/ml", torchdir .. "packages/nn", torchdir .. "packages/random"})
