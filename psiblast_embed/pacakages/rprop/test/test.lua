require "sparse"
require "rprop"

if false then
  sigma=100

  print("loading txt..")
  sp = sparse.read_txt("/mnt/local/iain/rankprop/rankprop2_matrix.txt",1)
  print("normalising...")
  rprop.sparse_normalise(sp,sigma)
  print("saving binary..")
  sparse.save_bin(sp,"/mnt/local/iain/rankprop/rankprop2_matrix.spbin",1) 
  sp=nil
  collectgarbage()

  print("loading txt..")
  sp = sparse.read_txt("/mnt/local/iain/rankprop/rankprop2_scop_matrix.txt",1)
  print("normalising...")
  rprop.sparse_normalise(sp,sigma)
  print("saving binary..")
  sparse.save_bin(sp,"/mnt/local/iain/rankprop/rankprop2_scop_matrix.spbin",1) 
  sp=nil
  collectgarbage()

end

print("loading normalised binaries..")
sp = sparse.read_bin("/mnt/local/iain/rankprop/rankprop2_matrix.spbin",1)
print("loaded, size:",sp:size())

print("rankprop init..")
rprop.init(sp:size())

--[[ 
--query style
qsp = sparse.read_bin("/mnt/local/iain/rankprop/rankprop2_scop_matrix.spbin",1)
print("loaded, size:",qsp:size())
--do fiddle substitution if qsp[qi] to end of sp[]
rprop.init(sp:size()+1) 
]]--

print("rankprop execute..")
f=io.open("test_out.txt","w")
f:close()
for qi=1,7329 do
  print("query:",qi)
  rprop.rankprop(sp,qi)
end

print("rankprop clean..")
rprop.clean()


