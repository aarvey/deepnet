require "sparse"
require "rprop"

qi_start=1
qi_end=7329
sigma=100
ext="txt"
prep=false

if #arg<2 then 
  print("usage: lua rankprop.lua qi_start qi_end [sigma(100)] [ext] [prep]")
  os.exit(1)
end

if arg[1] then qi_start=tonumber(arg[1]) end
if arg[2] then qi_end=tonumber(arg[2]) end
if arg[3] then sigma=tonumber(arg[3]) end
if arg[4] then ext=ext..arg[4] end
if arg[5] then prep=true end

out_fname=string.format("../data/test_result_sigma%d_%d_%d.%s",sigma,qi_start,qi_end,ext)

if prep==true then 

  print("loading txt..")
  sp = sparse.read_txt("/mnt/local/iain/rankprop/rankprop2_matrix.txt",1)
  print("normalising...")
  rprop.sparse_normalise(sp,sigma)
  print("saving binary..")
  sparse.save_bin(sp,"/mnt/local/iain/rankprop/rankprop2_matrix_normalised_sigma"..tostring(sigma)..".spbin",1) 
  sp=nil
  collectgarbage()

  print("loading txt queries")
  sp = sparse.read_txt("/mnt/local/iain/rankprop/rankprop2_scop_matrix.txt",1)
  print("saving binary (unnormalised) - use for e-value homologs..")
  sparse.save_bin(sp,"/mnt/local/iain/rankprop/rankprop2_scop_matrix.spbin",1) 
  sp=nil
  collectgarbage()

  os.exit(1)
end

print("loading normalised binary..")
sp = sparse.read_bin("/mnt/local/iain/rankprop/rankprop2_matrix_normalised_sigma"..tostring(sigma)..".spbin",1)
print("loaded, size:",sp:size())

print("rankprop init..")
rprop.init(sp:size())

print("rankprop execute..")
f=io.open(out_fname,"w") f:close() -- clear
for qi=qi_start,qi_end do
  print("query:",qi)
  rprop.query_init(sp,qi)
  -- rprop.query_add_homologs(spq:element(qi)) -- yhold[{ev< 0.001}] and y[]=1-ev
  rprop.query_rankprop(sp,qi,out_fname)
end

print("rankprop clean..")
rprop.clean()

--[[ 
--query style

print("loading unnormalised queries")
spq= sparse.read_bin("/mnt/local/iain/rankprop/rankprop2_scop_matrix.spbin",1)

--##fast
--load normalised
--load unnorm query
--do fiddle substitution if qsp[qi] to end of sp[]
--get unnormalised rows for added edges by scanning unnorm
--add edges to unnormalised rows
--normalise new rows
--rankprop
--remove query
--get normalised rows 

--##slow
--load unnormalised
--load unnormalised query
--add query
--add query edges
--normalise
--rankprop

rprop.init(sp:size()+1) 
]]--
