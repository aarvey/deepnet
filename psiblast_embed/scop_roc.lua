
require "lab"

function calc_roc(fx,cutoff)
  local area=0 
  local height=0 
  local fps=0 
  local numpos=0
  for i,p in ipairs(fx) do
    if p.y==1 then numpos=numpos+1 end
  end
  for i,p in ipairs(fx) do
    if p.y==1 then height=height+1 end
    if p.y==-1 then 
       area=area+height
       fps=fps+1
       if cutoff<=fps then break end
    end
  end
  local roc=area/(fps*numpos)
  return roc
end

function load_label_file(fname)

  f=io.open(fname)
  all={}
  all_dict={}
  while 1 do
    l=f:read()
    if not l then break end
    t={}
    t.i,t.name,labels=string.match(l,"(%S+)%s+(%S+)%s+(.*)")
    t.l={}
    for s in string.gmatch(labels,"(%S+)") do
       t.l[#t.l+1]=tonumber(s)
    end
    all[#all+1]=t
    all_dict[t.name]=t
  end
  return all,all_dict

end

function build_scop_score_matrix( labels )

  local score_matrix=torch.Tensor(7329,7329)

  for qi=1,7329 do
    -- build table for ROC calculation..
    for i = 1,7329 do
      -- if homology problem
      local ignore=false

      local lbls =labels[i].l
      local lbls2=labels[qi].l
      if lbls[2] == lbls2[2] then -- same fold
        if lbls[1] ~= lbls2[1] then -- different sfam
          -- ignore this comparison
  	  ignore=true
        end
      end
      local y
      if ignore==true then
	y=0 
      else
        if lbls[1] == labels[qi].l[1] then 
          y=1
        else 
          y=-1
        end
      end
      score_matrix[qi][i]=y
    end
  end --for qi
  return score_matrix
end


function scop_compute_roc( qi, ind, cutoff )

  local area=0 
  local height=0 
  local fps=0 
  local numpos=0
  for i=1,ind:size(1) do
    local y=scop_score_matrix[qi][ind[i]]
    if y==1 then numpos=numpos+1 end
  end
  for i=1,ind:size(1) do
    local y=scop_score_matrix[qi][ind[i]]
    if y==1 then height=height+1 end
    if y==-1 then 
       area=area+height
       fps=fps+1
       if cutoff<=fps then break end
    end
  end
  return area/(fps*numpos)
end

function compute_roc( labels, qi, pred, cutoff )

  if qi==476 then
    print("qi=476 itis")
  end
  -- build table for ROC calculation..
  local fx={}
  for i,p in ipairs(pred) do
    -- if homology problem
    local ignore=false

    local lbls =labels[i].l
    local lbls2=labels[qi].l
    if lbls[2] == lbls2[2] then -- same fold
      if lbls[1] ~= lbls2[1] then -- different sfam
        -- ignore this comparison
	ignore=true
      end
    end
    if ignore==true then
      --print("leaving "..tostring(i).." out")
    else
      local y
      if lbls[1] == labels[qi].l[1] then 
        y=1
      else 
        y=-1
      end
      fx[#fx+1]={i=i,fx=p,y=y} 
    end
  end
  if qi==476 then
    print("qi=476, hmm")
  end

  table.sort( fx, function(a,b) return a.fx>b.fx end )
  if qi==476 then
    print("qi=476, hmm2")
  end

  local area=0 
  local height=0 
  local fps=0 
  local numpos=0
  for i,p in ipairs(fx) do
    if p.y==1 then numpos=numpos+1 end
  end
  for i,p in ipairs(fx) do
    if p.y==1 then height=height+1 end
    if p.y==-1 then 
       area=area+height
       fps=fps+1
       if cutoff<=fps then break end
    end
  end
  if qi==476 then
    print("qi=476, hmm3")
  end

  local roc=area/(fps*numpos)
  return roc
end

------------------------------------------------------------------

labels,labels_dict = load_label_file("../data/scop.all.labs")

test_idx={}
test_idx_dict={}
f=io.open("../data/scop.test.labs")
while 1 do
 local l=f:read() if not l then break end
 local s=string.match(l,"(%S+)")
 test_idx[#test_idx+1]=tonumber(s)
 test_idx_dict[tonumber(s)]=true
end
f:close()

if false then
  print("building scop score matrix..")
  scop_score_matrix=build_scop_score_matrix(labels)
  print("saving score matrix")
  f=torch.DiskFile("../data/scop_score_matrix.torchbin","w")
  f:binary()
  f:writeObject(scop_score_matrix)
  f:close()
end

print("loading score matrix")
f=torch.DiskFile("../data/scop_score_matrix.torchbin","r")
f:binary()
scop_score_matrix=f:readObject()
f:close()

