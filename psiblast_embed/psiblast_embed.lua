
--psiblast embedding

require "nn"
require "sparse"

dofile("utils/args.lua")

compulsory_args={
  {db="SCOP", __valid={"SCOP","SCOPSWISS"} }
}
optional_args={ 
  {norm_eval_cutoff=0.0},
  {eval_matrix_out="nil"}
}
pargs=read_args (compulsory_args,optional_args)

-- globals
rocN=50
random.manualSeed(0)
math.randomseed(0)

-- load data


require "rprop"


if pargs.db=="SCOP" then
  sp=sparse.read_bin("../data/scop_matrix.spbin")
  rprop.sparse_normalise(sp,100)
  learning_rate=0.01
elseif pargs.db=="SCOPSWISS" then
  if false then
    print("reading..")
    sp=sparse.read_bin("../../rp2/data/rankprop_matrix.spbin")
    print("applying rprop normalise..")
    rprop.sparse_normalise(sp,100)
    print("saving normalised done")
    sparse.save_bin(sp,"../../rp2/data/rankprop_matrix_norm_100.spbin",1)
    sp=nil
    collectgarbage()
  end
  print("reading db..")
  sp=sparse.read_bin("../../rp2/data/rankprop_matrix_norm_100.spbin")
  learning_rate=0.01
else
  error("unknown db")
end

-- labels, test idx,  and scop roc functions
dofile("scop_roc.lua")


-- set up 2 mlp's that share the save weights and bias
pr_mlp={}
for i=1,2 do
  pr_mlp[i]=nn.Sequential()
  pr_mlp[i]:add( nn.SparseLinear(sp:size(),100) )
end
for i=1,pr_mlp[1]:size() do
  pr_mlp[2]:get(i).weight:set(pr_mlp[1]:get(i).weight)
  pr_mlp[2]:get(i).bias:set(pr_mlp[1]:get(i).bias)
end

--make parallel table that takes pair of examples as input
prl = nn.ParallelTable()
prl:add(pr_mlp[1])
prl:add(pr_mlp[2])

-- now we define our top level network that takes this parallel table and computes the pairwise distance betweem
-- the pair of outputs
mlp= nn.Sequential()
mlp:add(prl)
mlp:add(nn.PairwiseDistance(1))

-- and a criterion for pushing together or pulling apart pairs
crit=nn.HingeEmbeddingCriterion(1)

-- Use a typical generic gradient update function
function gradUpdate(mlp, x, y, criterion, learningRate)
  local pred = mlp:forward(x)
  local err = criterion:forward(pred, y)
  local gradCriterion = criterion:backward(pred, y)
  mlp:zeroGradParameters()
  mlp:backward(x, gradCriterion)
  mlp:updateParameters(learningRate)
end

--xsp=torch.Tensor(2,10000)
--xsp1=torch.Tensor(2,10000)
--xsp2=torch.Tensor(2,10000)

xsp=torch.Tensor()
xsp1=torch.Tensor()
xsp2=torch.Tensor()

for epoch=0,40 do
if epoch > 0 then
 epoch_togdist=0
 epoch_ntogdist=0
 print("epoch:",epoch)
 for i=1,10000 do 

  if math.random(100)<50 then

    -- find two similar
    local count=0
    while true do
      i1=math.random(sp:size())
      sp:element(i1):toTensor(xsp1)
      if xsp1:dim()==2 then
        j1 = math.random(xsp1:size(2))
  
        i2=xsp1[2][j1]
        e =xsp1[1][j1]
        sp:element(i2):toTensor(xsp2)
  
        if e>pargs.norm_eval_cutoff then
          break
        end
        count=count+1
        if count>99999 then
          error("probably inifinite loop")
        end 
      else
--        error("element:"..tostring(i1).." xsp1:dim():"..tostring(xsp1:dim()))
      end
    end

    together=1 
    lr= learning_rate
    --old - normalisation =0->1 lr= learning_rate* math.exp(-e/10000)
   

  else
    -- push 2 random apart

  -- replace sparse to tensor with inplace tensor with resize

    i1=math.random(sp:size())
    sp:element(i1):toTensor(xsp1)

    i2=math.random(sp:size())
    sp:element(i2):toTensor(xsp2)

    together=-1 
    lr= learning_rate
  end
  
  gradUpdate(mlp,{xsp1,xsp2},together,crit,lr)

  dist=mlp:forward({xsp1,xsp2})[1]
  if together==1 then 
    epoch_togdist=epoch_togdist+dist
  else
    epoch_ntogdist=epoch_ntogdist+dist
  end

  if i%1000==0 then 
    collectgarbage()
  end

 end
 print("epoch",epoch,"epoch_togdist:",epoch_togdist,"epoch_ntogdist:",epoch_ntogdist)

end --if epoch>0

  --scop roc evaluate
  if true then -- epoch%5==0 then

   -- get embedded vectors for all SCOP
   --print("embedding scop..")
   ex={}
   for j=1,7329 do
     sp:element(j):toTensor(xsp)
     xout=pr_mlp[1]:forward(xsp)
     ex[#ex+1]=torch.Tensor(xout:size(1)):copy(xout)
   end

   --print("computing roc scores..")
   roc1_av=0
   roc50_av=0
   dist=torch.Tensor(7329)

   if pargs.eval_matrix_out ~="nil" then
     f=io.open(pargs.eval_matrix_out,"w") 
     for i=1,7329 do 
       f:write(string.format("%d ",i))
       for j=1,7329 do -- 1 norm..
         d=lab.dist(ex[j],ex[i],1) --+math.random()*0.000001
         f:write(string.format("%g ",1-d))
       end
       f:write("\n")
     end
     f:close()
   end

   for i=1,#test_idx do
     qi=test_idx[i]
     for j=1,7329 do -- 1 norm..
       dist[j]= lab.dist(ex[j],ex[qi],1) 
     end
     sdist_r,sind=lab.sort(dist)
     roc1  = scop_compute_roc( qi, sind, 1 )
     roc50 = scop_compute_roc( qi, sind, 50 )
     roc1_av =roc1_av+roc1
     roc50_av=roc50_av+roc50
     if i%1000==0 then 
       collectgarbage()
     end
   end
   print("epoch:", epoch, "roc50:",roc50_av/#test_idx, "roc1:",roc1_av/#test_idx)

  end -- if roc evaluate

end




