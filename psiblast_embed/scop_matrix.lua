
-- make a matrix with only scop vs scop in it
-- load rankprop2_matrix and chop out anything > 7329

--make binaries to speed things up

--do this once

require "sparse"

if false then 
--if false then
  sp = sparse.read_txt("../../rp2/data/rankprop_matrix.txt",1)
  sparse.save_bin(sp,"../../rp2/data/rankprop_matrix.spbin",1)
end

if true then 
--if false then
  sp = sparse.read_bin("../../rp2/data/rankprop_matrix.spbin",1)
  f=io.open("../data/scop_matrix.txt","w")
  for i=1,7329 do
    x=sp:element(i):toTensor()
    nscop =0
    for j=1,x:size(2) do
      if x[2][j]<=7239 then 
        nscop=nscop+1 
      end
    end
    f:write(string.format("%d %d\n",i,nscop))
    for j=1,x:size(2) do
      if x[2][j]<=7239 then 
        f:write(string.format("%d ",x[2][j]))
      end
    end
    f:write("\n")
    for j=1,x:size(2) do
      if x[2][j]<=7239 then 
        f:write(string.format("%g ",x[1][j]))
      end
    end
    f:write("\n")
  end
  f:close()
end

if true then 
--if false then
  sp = sparse.read_txt("../data/scop_matrix.txt",1)
  sparse.save_bin(sp,"../data/scop_matrix.spbin",1)
end
