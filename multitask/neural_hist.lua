-- Get the system modules
require("torch")
require("nn")
require("math")
require("os")

-- Get the utilities for read, parsing, etc
dofile("../utils/cv.lua")
dofile("../utils/dirs.lua")
dofile("../utils/base.lua")
dofile("../utils/args.lua")
dofile("../utils/fasta.lua")
dofile("../utils/blosum.lua")
dofile("../utils/membership.lua")
dofile("../utils/model.lua")
dofile("../utils/draw.lua")
dofile("../utils/print.lua")
dofile("../utils/examples.lua")

-- Get the aux functions
dofile("./mlp.lua")
dofile("kmers.lua")


runNumber = tonumber(arg[1])
datafile = arg[2]
maxIteration = tonumber(arg[3])
HUs = tonumber(arg[4])
embedsize = tonumber(arg[5])
kmersize = tonumber(arg[6])
mlpstyle = tonumber(arg[7])
positive_overweight = tonumber(arg[8])

print('HUs: '..HUs, 'embedsize: '..embedsize)

math.randomseed(runNumber)

--------------------------------------------------
------- Take care of command line params ---------
--------------------------------------------------

fasta, motifStrs = motif_fasta_load(datafile)

------------------------------------------
------- Get trainset  -------
------------------------------------------

pve = torch.Tensor(2)
pve[1] = 1
pve[2] = 0
nve = torch.Tensor(2)
nve[1] = 0
nve[2] = 1


print("size of labels:", pve:size()[1], nve:size()[1])
local trainset = fasta_to_dataset(fasta, 5, false)
print("Size of trainset is " .. #trainset)
print("Size of first train example is:" .. trainset[1].s:len())

for i=1,#trainset do
  trainset[i].example = example_to_kmers(trainset[i].s, kmersize)
end

------------------------------------------
-------------- Run on trainset -----------
------------------------------------------

mlp, layers = getmlp(trainset,HUs,kmersize,embedsize,mlpstyle)

function iscloser(pred, lab1, lab2) 
  dist1 = (pred[1] - lab1[1])^2 + (pred[2] - lab1[2])^2
  dist2 = (pred[1] - lab2[1])^2 + (pred[2] - lab2[2])^2
  if dist1 < dist2 then
    return true
  end
  return false

  --d = pred:nDimension()
  --for i=1,d do 
  --end
end


function updatemlp(mlp, criterion, input, target, currentLearningRate)
  criterion = nn.MSECriterion()
  err = criterion:forward(mlp:forward(input), target)
  mlp:zeroGradParameters()
  mlp:backward(input, criterion:backward(mlp.output, target))
  if target == nve then
    mlp:updateParameters(currentLearningRate/positive_overweight)
  else 
    mlp:updateParameters(currentLearningRate)
  end
  return err
end

-- Taken from stochastic gradient
function mytrain(trainset, mlp)
  local iteration = 1
  local learningRate = 0.01
  local currentLearningRate = learningRate
  local learningRateDecay = 0.01
  local weightDecay = 0.0001

  print("# StochasticGradient: training")
  while true do
    local currentError = 0
    local err = 0
    local input 
    local target
    local shuffledIndices = lab.randperm(#trainset)
    for idx = 1,#trainset do
      i = shuffledIndices[idx]

      input = trainset[i].example
      target = trainset[i].label
      err = updatemlp(mlp, criterion, input, target, currentLearningRate)
      currentError = currentError + err
    end
    currentError = currentError / (#trainset)
    --testErrorStr = twoclass_error(mlp, testset)
    testErrorStr = "Fix this"
    print("Iter #" .. iteration .. " TrainErr=" .. currentError .. 
          "  TestErr=" .. testErrorStr .. 
          " lr=" .. currentLearningRate)
    iteration = iteration + 1
    currentLearningRate = learningRate/(1+iteration*learningRateDecay)
    if maxIteration > 0 and iteration > maxIteration then
      print("# StochasticGradient: you have reached the maximum number of iterations")
      break
    end
  end

  embedlayer = mlp:get(1)
  --print("Get top 50")
  --show_kmer_weights(embedlayer,50,true)
  --print("Get top 4000")
  --show_kmer_weights(embedlayer,4000,true)
  --os.exit()
  return mlp
end


function do_loov(trainset, mlp, trainfunc, errorfunc)
  local trainset_copy = {}
  for i=1,#trainset do
    trainset_copy[i] = trainset[i]
  end
  for i=1,#trainset do
    -- Set up the training set less one example
    local newtrainset = {}
    for j=1,#trainset_copy do
      if j==i then
        -- do nothing
      elseif j>i then
        newtrainset[j-1] = trainset_copy[j]
      elseif j<i then
        newtrainset[j] = trainset_copy[j]
      end
    end

    -- Train mlp on the training set
    mlp, layers = getmlp(trainset,HUs,kmersize,embedsize,mlpstyle)
    --print(mlp)
    --mlp = trainfunc(newtrainset, layers)
    mlp = trainfunc(newtrainset, mlp)
    --trainErrorStr = errorfunc(mlp, newtrainset)
    trainErrorStr = "Fix this 2"
    local testset = {}
    testset[1] = trainset_copy[i]
    --print('Testset is:', testset[1].s, testest[1].label)
    testErrorStr = errorfunc(mlp, testset)
    print(i, trainErrorStr, testErrorStr)
    --write_mlp(mlp, layers)
  end
end


mlp = do_loov(trainset, mlp, mytrain, twoclass_error)

