require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("/home/aarvey/.path.lua")
dofile(path.utils .. "/draw.lua")

function encode1hot(numpoints) 
  indices = {}
  for i=1,numpoints do
    indices[i] = torch.Tensor(numpoints)
    indices[i][i] = 1
  end
  return indices
end


function getsimmatrix(simtable,numpoints)
  local simmatrix = {}
  local i = 0
  local j = 0
  for k=1,#simtable do
    i = simtable[k][1]
    j = simtable[k][2]
    if simmatrix[i] == nil then
      simmatrix[i] = {}
    end
    if simmatrix[j] == nil then
      simmatrix[j] = {}
    end
    simmatrix[i][j] = 1
    simmatrix[j][i] = 1
    simmatrix[i][i] = 1
    simmatrix[j][j] = 1
  end
  for i=1,numpoints do
    if simmatrix[i] == nil then
      simmatrix[i] = {}
    end
    simmatrix[i][i] = 1
  end
  return simmatrix
end

function getposexamples(simtable, numpoints, data)
  posexamples = {}
  local k = 1
  for l=1,#simtable do 
    i = simtable[l][1]
    j = simtable[l][2]
    posexamples[k] = {}
    posexamples[k][1] = {data[i], data[j]}
    posexamples[k][2] = 1
    k = k + 1
  end
  return posexamples
end

function getnegexamples(simmatrix, numpoints, data)
  k = 1
  negexamples = {}
  for i=1,numpoints do 
   if simmatrix[i]==nil then
      simmatrix[i] = {}
    end
  end
  for i=1,numpoints do
    for j=i+1,numpoints do
      if simmatrix[i][j]==nil then 
        negexamples[k] = {}
        negexamples[k][1] = {data[i], data[j]}
        negexamples[k][2] = -1
        k = k +1
      end
    end
  end
  return negexamples
end




function mlp_from_file(fname) 
  mlp_fname = fname
  ext = mlp_fname:sub(mlp_fname:len()-2, mlp_fname:len())
  if ext == ".gz" then
    cmd = "gzip -d " .. mlp_fname
    os.execute(cmd)
    mlp_fname = mlp_fname:sub(1,mlp_fname:len()-3)
  end
  print("Reading embedding mlp from " .. mlp_fname)
  f = torch.DiskFile(mlp_fname, "r")
  mlp = f:readObject()
  f:close()
  return mlp
end

function getembedmlp(indim, outdim, fname, layer_hus)
  -- A table for two mlp's that will share weights and bias
  pr_mlp={}

  print("Layer hus has elts: ", #layer_hus)
  for i=1,#layer_hus do
    print("layer hu is:", layer_hus[i].dim)
  end

  new_layer_hus = {}
  new_layer_hus[1] = {}
  new_layer_hus[1].dim = indim
  for i=1,#layer_hus do
    new_layer_hus[i+1] = {}
    new_layer_hus[i+1].dim = layer_hus[i].dim
  end
  new_layer_hus[#layer_hus+2] = {}
  new_layer_hus[#layer_hus+2].dim = outdim

  layer_hus = new_layer_hus

  -- Set up the two mlp's
  --   Both will be sequential
  --   Both will have a linear representation
  --   The input and output spaces of the Linear layer
  for i=1,2 do
    pr_mlp[i]=nn.Sequential()
    
    for j=1,#layer_hus-1 do    
      lin = nn.Linear(layer_hus[j].dim, layer_hus[j+1].dim)
      lin.weightDecay=1e-10
      pr_mlp[i]:add(lin)
      print("adding layer", lin, " with dims ", layer_hus[j].dim, layer_hus[j+1].dim, "to pr_mlp[i]", i)
    end
  end

  print("pr_mlp[1] size is:", pr_mlp[1]:size())

  if fname then
    full_mlp = mlp_from_file(fname)
    pr_mlp[1] = full_mlp:get(1):get(1)
    print("full mlp size is:", full_mlp:size())
  end

  print("pr_mlp[1] size is:", pr_mlp[1]:size())
  -- For each layer of pr_mlp1, go through and set the weight & bias to
  -- be the same.

  for i=1,pr_mlp[1]:size() do
    print("pr_mlp[2] size is:", pr_mlp[2]:size())
    print("pr_mlp[2]:get(i) size is:", pr_mlp[2]:get(i))
    if pr_mlp[2]:get(i) ~= nil then 
      pr_mlp[2]:get(i).weight:set(pr_mlp[1]:get(i).weight)
      pr_mlp[2]:get(i).bias:set(pr_mlp[1]:get(i).bias)
    end
  end

  -- Get pairs of examples as input.  The pairs will be points, the
  -- target (see below) will be if they are related or not.
  prl = nn.ParallelTable()
  prl:add(pr_mlp[1])
  prl:add(pr_mlp[2])

  -- Put the mlps into a larger mlp with a pairwise distance to measure
  -- how bad we're doing

  mlp= nn.Sequential()
  mlp:add(prl)
  mlp:add(nn.PairwiseDistance(1))

  return mlp
end


function getembedlayer(mlp)
  return mlp:get(1):get(1)
end



-- Use a typical generic gradient update function
function gradUpdate(mlp, x, y, criterion, currentLearningRate)
  local pred = mlp:forward(x)
  local err = criterion:forward(pred, y)
  local cutoff =  1000
  local maxcutoff = 1000000
  local incrcutoff = 0.5
  if err > cutoff and err < maxcutoff then
    currentLearningRate = currentLearningRate / 4
  elseif err > maxcutoff and currentLearningRate > 10^(-10) then
    currentLearningRate = 10^(-10)
  else
    if err < incrcutoff then
      currentLearningRate = currentLearningRate * (1.1)
    end
  end
  --print('Err is:', err)
  --print('Current leanring rate is:', currentLearningRate)
  local gradCriterion = criterion:backward(pred, y)
  mlp:zeroGradParameters()
  mlp:backward(x, gradCriterion)
  mlp:updateParameters(currentLearningRate)
  return err, currentLearningRate
end



function trainembedding(mlp, criterion, indices, rawdata, posexamples, negexamples, fname, equal_pos_neg)
  
  print("Number of examples: " .. #posexamples+#negexamples)
  print("Number of positive examples: " .. #posexamples)
  print("Number of negative examples: " .. #negexamples)

  local lr = 0.0001
  local lrdecay = 0.001
  local lrinit = lr
  numepochs=200
  start_epoch = 1

  lr_mult = (#posexamples / #negexamples)  * 2
  neg_increment = math.floor(#negexamples / #posexamples / 5)

  print("learning rate multiplier for negative examples is: " .. lr_mult)

  shuffledPosEx = {}
  shuffledNegEx = {}
  for j=1,numepochs do
    posIdx = lab.randperm(#posexamples)
    for i=1,#posexamples do
      shuffledPosEx[i] = posexamples[posIdx[i]]
    end
    negIdx = lab.randperm(#negexamples)
    for i=1,#negexamples do
      shuffledNegEx[i] = negexamples[negIdx[i]]
    end
    posidx = 1
    negidx = 1
    totalPosErr = 0
    totalNegErr = 0
    while true do 
      if math.random(1,#posexamples+#negexamples) < #posexamples or negidx > #negexamples then
        if posidx <= #posexamples then 
          err, lr = gradUpdate(mlp, shuffledPosEx[posidx][1], shuffledPosEx[posidx][2], criterion, lr)
          totalPosErr = totalPosErr + err
        end
        posidx = posidx + 1
        if equal_pos_neg then
          negidx = negidx + neg_increment
        end
      else 
        if negidx <= #negexamples then 
          err, lr = gradUpdate(mlp, shuffledNegEx[negidx][1], shuffledNegEx[negidx][2], criterion, lr * lr_mult)
          totalNegErr = totalNegErr + err
        end 
        negidx = negidx + 1
      end      
      if lr > lrinit or lr < 10^(-30) then
        lr = lrinit / (1 + j*lrdecay)
      end
      if negidx % 5000 == 0 then
        print("Learning gradient from point " .. posidx, negidx)
        print("PosError="..totalPosErr, "NegError="..totalNegErr) 
      end

      if negidx > #negexamples and posidx > #posexamples then break end
    end

    lr = lr / (1 + j*lrdecay)

    print("===========================================================")
    print("numepochs="..j, "lr="..lr, "perr="..totalPosErr, "nerr="..totalNegErr)
    print("===========================================================")

    basefname = fname..".epoch" .. j+start_epoch ..  "_numEx" .. #posexamples .. "_hus20" .. "_outdim" .. outdim
    modelfname = basefname .. ".mlp"

    f = torch.DiskFile(modelfname, "w")
    f:writeObject(mlp) 
    f:close()
    os.execute("gzip -f " .. modelfname)

    if numpoints == nil then
      numpoints = #indices
    end

    embedfname = basefname .. ".ebd"
    f = io.open(embedfname, "w")
    for k=1,numpoints do
      if rawdata ~= nil then
        embed = mlp:get(1):get(1):forward(rawdata[k])
      else
        embed = mlp:get(1):get(1):forward(indices[k])
      end
      --print(embed:size())
      outline = names[k] .. ": "
      for ek=1,embed:size(1) do
        outline = string.format("%s%.7f ",  outline, embed[ek])
      end
      outline = outline:sub(1,outline:len()-1) .. "\n"
      --print("outline:", outline)
      f:write(outline)
    end
    f:flush()
    f:close()

     
    maxx = -999
    minx = 999
    maxy = -999
    miny = 999

    circles = {}    
    for k=1,numpoints do
      if rawdata ~= nil then
        embed = mlp:get(1):get(1):forward(rawdata[k])
      else
        embed = mlp:get(1):get(1):forward(indices[k])
      end
      --print(embed:size())
      x = embed[1]
      y = embed[2]
      circles[k] = {}
      circles[k].x = x
      circles[k].y = y
      if x > maxx then maxx = x end
      if y > maxy then maxy = y end
      if y < miny then miny = y end
      if x < minx then minx = x end
    end

    -- Normalize x, y of circle
    if miny > 0 then miny = 0 end
    if minx > 0 then minx = 0 end

    --for k=1,numpoints do
      --print("before x,y", circles[k].x, circles[k].y)
      --circles[k].x = ((circles[k].x + -minx) / (maxx-minx)) * winsize
      --circles[k].y = ((circles[k].y + -miny) / (maxy-miny)) * winsize
      --print("after x,y", circles[k].x, circles[k].y)
    --end

    --show_clusters(circles, names)
    --if w~=nil and cr~=nil then
    --  clear(w,cr)
    --  drawgraph(w,cr,circles,simtable)
    --end
    --cr:sleep(1)
  end
end


function lpdist(x,y,p) 
  local dist = 0
  if #x~=#y or p <= 0 then
    print("Error with lpdist, need better params")
  end
  for d=1,#x do
    dist = dist + (math.abs(x[d] - y[d]))^p
  end
  dist = dist^(1/p)
  return dist
end

function show_clusters(circles, names)
  local dist = {}
  local c1 = {}
  local c2 = {}
  local nameslisted = {}
  local numclose = 0

  for i=1,#circles do
    dist[i] = {}
    for j=i+1,#circles do
      c1 = {circles[i].x, circles[i].y}
      c2 = {circles[j].x, circles[j].y}
      dist[i][j] = lpdist(c1, c2, 2)
      if dist[i][j] < 100 and (nameslisted[names[j]] == nil or nameslisted[names[j]] < 3) then
          if nameslisted[names[j]] == nil then nameslisted[names[j]]=0 end
          nameslisted[names[j]] = nameslisted[names[j]] + 1
          --print(adjmatrix[i][j], "d("..names[i]..","..names[j]..") = "..dist[i][j], "("..c1[1]..","..c1[2]..")", "("..c2[1]..","..c2[2]..")")
          numclose = numclose + 1
        end
    end
  end
  print("Num close:", numclose)
end