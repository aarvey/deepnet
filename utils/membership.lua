
function membership_load(fname)
	local f=io.open(fname)
	local l=f:read()
	local labels = {}
	local ls=string.sub(l,4)
	for s in string.gmatch(string.sub(l,4).." ","%S+") do
		labels[#labels+1]=s
	end
	local examples={}
	while l do 
		l=f:read() if not l then break end
		local e = {name=string.match(l,"%S+"),member={}}
		local lm = string.sub(l,string.len(e.name)+1)
		for s in string.gmatch(lm.." ","%S+") do
			e.member[#e.member+1]=tonumber(s)
		end
		examples[#examples+1]=e
	end
	f:close()
	return labels,examples
end
