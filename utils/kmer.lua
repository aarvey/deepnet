------------------------------------------
-------- Turn trainset into kmers  -------
------------------------------------------
function kmer_to_idx(kmer,kmersize) 
  idx = 0
  --print('kmer', kmer)
  for i=1,kmer:len() do
    c = kmer:sub(i,i)
    --print('c',c)
    idx = (idx*4) + (bp_to_num(c)-1)
  end
  return idx 
end

function idx_to_kmer(idx,kmersize)
  kmer = {}
  for i=1,kmersize do
    c = num_to_bp((idx % 4) + 1)
    kmer[#kmer+1] = c
    --print('idx', idx, 'c', c)
    idx = math.floor(idx/4)
  end
  revkmer = {}
  for i=1,kmersize do
    revkmer[i] = kmer[kmersize-i+1]
  end
  return table.concat(revkmer)
end


--print(kmer_to_idx('A'))
--print(idx_to_kmer(kmer_to_idx('A'), 1))
--print(idx_to_kmer(kmer_to_idx('TTTT'), 4))
--print(idx_to_kmer(kmer_to_idx('TTTAAA'), 6))
--os.exit(2)


function rev_comp(seq) 
  seq = seq:reverse()
  comp_seq = {}
  for i=1,seq:len() do
    comp_seq[i] = comp_bp(seq:sub(i,i))
  end
  return table.concat(comp_seq)
end


function example_to_kmers(ex_s,kmersize) 
  --print('s', ex_s)
  ex_tensor = torch.Tensor(4^kmersize)
  ex_tensor:zero()
  num_kmers = #ex_s-kmersize+1
  for i=1,num_kmers do

    kmer = ex_s:sub(i,i+kmersize-1)
    --print('kmer',kmer)
    idx = kmer_to_idx(kmer) + 1
    --print('idx', idx)
    ex_tensor[idx] = ex_tensor[idx] + 1

    revcomp = rev_comp(kmer)
    --print('rev kmer',revcomp)
    idx = kmer_to_idx(revcomp) + 1
    --print('idx', idx)
    ex_tensor[idx] = ex_tensor[idx] + 1

  end
  for i=1,ex_tensor:size(1) do
    ex_tensor[i] = ex_tensor[i] / num_kmers * 1000
  end
  return ex_tensor
end

-- test kmer_to_idx
--print(kmer_to_idx('A'))
--print(kmer_to_idx('C'))
--print(kmer_to_idx('G'))
--print(kmer_to_idx('T'))
--print(kmer_to_idx('AA'))
--print(kmer_to_idx('AC'))
--print(kmer_to_idx('AG'))
--print(kmer_to_idx('AT'))
--print(kmer_to_idx('CA'))
--print(kmer_to_idx('CC'))
--print(kmer_to_idx('CG'))
--print(kmer_to_idx('CT'))
--print(kmer_to_idx('GA'))
--print(kmer_to_idx('GT'))
--print(kmer_to_idx('T'))
--print(kmer_to_idx('TT'))
--print(kmer_to_idx('TTT'))
--print(kmer_to_idx('TTTT'))
--print(kmer_to_idx('TTTTT'))
--print(kmer_to_idx('TTTTTT'))

--os.exit(2)


function show_kmer_weights(embedlayer,numtop,descending) 
  allweights = embedlayer.weight
  print(allweights:size())
  tab10v = {}
  tab10i = {}
  for i=1,numtop do tab10v[i] = 0 end
  for i=1,numtop do tab10i[i] = {} end
  function findmin(tab) 
    local min = nil
    if descending then
      min = 99999999
    else
      min = -99999999
    end
    local min_idx = nil
    for i=1,#tab do
      local v = tab[i]
      if (descending and v < min) or (not descending and v > min) then
        min = v
        min_idx = i
      end
    end
    return min, min_idx
  end
  for i=1,allweights:size()[1] do
    for j=1,allweights:size()[2] do
        val = allweights[i][j]
        min, minidx = findmin(tab10v)
        --print('min', min)
        --print('val', val)
        if (descending and val > min) or (not descending and val < min) then
          tab10v[minidx] = val
          tab10i[minidx][1] = i
          tab10i[minidx][2] = j
        end
    end
  end
  --print(table.tostring(tab10v))
  --print(table.tostring(tab10i))
  i = 0
  for k,v in pairs(tab10i) do 
    i = i + 1
    print(tab10v[i], idx_to_kmer(v[1]-1, kmersize), v[1], v[2])
  end
end

