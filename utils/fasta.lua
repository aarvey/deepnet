-- Author: Aaron Arvey
-- Date: 05/21/2008
-- Note: This code inspired by code orignally written by Iain Melvin

dofile("/home/aarvey/.path.lua")
dofile(path.utils .. "/print.lua")

require("io")
function positions_load(pos_fname)
  local fin = assert(io.open(pos_fname, "r"))
  local ret = {}
  local line = ""
  while 1 do
    line = fin:read()
    if not line then break end
    local name,chrom,chr_start,chr_finish = string.match(line,">(%S+)%s+(%S+)%s+(%S+)%s+(%S+)")

    line=fin:read() 
    local seq_start,seq_finish = string.match(line,"(%S+)%s+(%S+)")
    ret[#ret+1]={name=name, chr=chrom, 
                 chr_start=chr_start, chr_finish=chr_finish, 
                 seq_start=seq_start, seq_finish=seq_finish}
  end
  fin:close()
  return ret
end



function fasta_to_dataset(fasta, numMotifs, usemotiflabels)
  local trainset = {}
  for i=1,#fasta do
    str = fasta[i].s
    trainset[#trainset+1] = {}
    trainset[#trainset].example = dna_to_tensor_binary(str)
    trainset[#trainset].s = str
    label = torch.Tensor(numMotifs)
    numpresent = 0
    for j=1,numMotifs do
      label[j] = fasta[i].ispresent[j]
      if label[j] > 0 then numpresent = numpresent + 1 end
    end
    if usemotiflabels then
      --print("using raw label")
      trainset[#trainset].label = label
    elseif numpresent > 0 then
      --print("using pos label")
      trainset[#trainset].label = pve
    else 
      --print("using neg label")
      trainset[#trainset].label = nve
    end
  end
  function trainset:possize() return #trainset end
  return trainset
end


function motif_fasta_load(fasta_fname)
  local motifStrs = {}
  local fin = assert(io.open(fasta_fname,"r"))
  local ret = {}
  local line = fin:read()
  local origCommentLine = ""
  while 1 do
    motif_ispresent = {}
    origCommentLine = line
    line = string.sub(line,2,string.len(line))
    --print("line is:", line)
    i = 0
    for word in line:gmatch("\(%S+,%S+,%S+,%S+\); ") do
      i = i + 1
      --print("Word is:", word)
      local motif, ispresent, idxstart, idxend = string.match(word,"(%S+),(%S+),(%S+),(%S+)")
      motif = motif:sub(2)
      --print(motif, ispresent, idxstart, idxend)
      motif_ispresent[i] = ispresent
      if table_count(motifStrs, motif) == 0 then
        motifStrs[#motifStrs+1] = motif
      end
    end
    s = ""
    while 1 do
      line=fin:read()
      if not line then break end
      if string.sub(line,1,1) == '>' then break end
      s=s..line
    end
    ret[#ret+1]={ispresent=motif_ispresent, s=s, comment=origCommentLine}
    --print(ret[#ret].ispresent[1], ret[#ret].ispresent[2], ret[#ret].s)
    if not line then break end
  end
  fin:close()
  return ret, motifStrs
end


function fasta_load(fasta_fname)
  local fin = assert(io.open(fasta_fname,"r"))
  local ret = {}
  local line = fin:read()
  while 1 do
    local name,chrom,start,finish = string.match(line,">(%S+)%s+(%S+)%s+(%S+)%s+(%S+)")
    s = ""
    while 1 do
      line=fin:read() 
      if not line then break end
      if string.sub(line,1,1) == '>' then break end
      s=s..line
    end
    ret[#ret+1]={name=name, chr=chrom, start=start, finish=finish, s=s}
    if not line then break end
  end
  fin:close()
  return ret
end


function bp_to_num(bp)
  if (bp=="A" or bp=="a") then
    return 1
  elseif (bp=="C" or bp=="c") then
    return 2
  elseif (bp=="G" or bp=="g") then
    return 3
  elseif (bp=="T" or bp=="t") then
    return 4
  elseif (bp=="N" or bp=="n") then
    return 5
  end
  return 6
end

function comp_bp(bp)
  if (bp=="A" or bp=="a") then 
    return "T"
  elseif (bp=="C" or bp=="c") then
    return "G"
  elseif (bp=="G" or bp=="g") then
    return "C"
  elseif (bp=="T" or bp=="t") then
    return "A"
  elseif (bp=="N" or bp=="n") then
    return "N"
  end
end

function num_to_bp(num)
  if (num==1) then
    return "A"
  elseif (num==2) then
    return "C"
  elseif (num==3) then
    return "G"
  elseif (num==4) then
    return "T"
  else
    return "N"
  end
end

function dna_to_tensor_binary(p)
  -- DNA string to binary tensor
  p = string.upper(p)
  lp = string.len(p)
  x=torch.Tensor(lp,4,1)
  x:zero()
  for i=1,lp do
    c=string.sub(p,i,i)
    vi=bp_to_num(c)
    x[i][vi][1]=1
  end
  return x
end

function dna_to_tensor_numeric( p )
  -- DNA string to binary tensor
  p = string.upper(p)
  lp = string.len(p)
  x=torch.Tensor(lp,1)
  x:zero()
  for i=1,lp do
    c=string.sub(p,i,i)
    vi=bp_to_num(c)
    x[i][1]=vi
  end
  return x
end
