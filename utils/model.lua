function save_model(fname)
  bin = torch.Archiver(fname, "w")
  bin:binary()
  bin:writeObject(mlp)
end

function load_model(fname)
  bin = torch.Archiver(fname, "r")
  bin:binary()
  mlp = bin:readObject()
end

