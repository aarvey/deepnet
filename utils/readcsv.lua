
-- Parse file format (feature vector & label)
-- Each feature is seperated by a comma
-- The label is the final feature and is followed by a semi-colon
function readcsv(fname, maxlines)
  points = {}
  i = 0
  indim = 0
  for line in io.lines(fname) do
    i = i + 1
    n = string.len(line)

    -- determine dimension space of points
    dim = 1
    for k=1,n do
      if string.sub(line,k,k)=="," then dim=dim+1 end
      if string.sub(line,k,k)==";" then break end
      if string.sub(line,k,k)=="\n" then break end
    end
    local point = torch.Tensor(dim)
    indim = dim

    -- get the point's values
    j = 0
    k = 0
    split_i = 1
    while (k<=n) do
      k = k + 1
      if (string.sub(line,k,k)=="," or string.sub(line,k,k)==";" or string.sub(line,k,k)=="\n") then
        j = j + 1
        --print(j, split_i, k-1, n, string.sub(line, split_i, k-1))
        point[j] = tonumber(string.sub(line, split_i, k-1))
        split_i = k+1
      end
    end
    points[i] = point
    --print(points[i][1],points[i][2])
    --print(line,point)
    if maxlines ~= nil and i > maxlines then
      return points, indim
    end
  end
  return points, indim  
end


function readtfs(fname, maxlines)
  points = {}
  i = 0
  indim = 0
  ii = 0
  for line in io.lines(fname) do repeat
    i = i + 1
    if (i-1) % 2 == 0 then
      ii = ii + 1
      points[ii] = {}
      points[ii].comment = line
      break
    end

    n = string.len(line)

    -- determine dimension space of points
    dim = 1
    for k=1,n do
      if string.sub(line,k,k)=="," then dim=dim+1 end
      if string.sub(line,k,k)==";" then break end
    end
    local point = torch.Tensor(dim)
    indim = dim

    -- get the point's values
    j = 0
    k = 0
    split_i = 1
    while (k<=n) do
      k = k + 1
      if (string.sub(line,k,k)=="," or string.sub(line,k,k)==";") then
        j = j + 1
        --print(j, split_i, k-1, n, string.sub(line, split_i, k-1))
        point[j] = tonumber(string.sub(line, split_i, k-1))
        split_i = k+1
      end
    end
    points[ii].example = point
    if maxlines ~= nil and i > maxlines then
      return points, indim
    end
  until true end
  return points, indim  
end
