function randperm(str)
  local result = {}
  local len = string.len(str)
  for i=1,len do
    r = math.random(1,len)    
    result[i] = string.sub(str,r,r)
  end
  return table.concat(result)
end


function negative_example(sidestr, len)
  while string.len(sidestr) < len + (len/5) do 
    sidestr = sidestr .. sidestr
  end
  str = randperm(sidestr)

  len = len + math.random(-len/5, len/5)
  str = string.sub(str, 1, len)
  return str
end

