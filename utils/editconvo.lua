function read_motifs(filename)
  local fin = assert(io.open(filename,"r"))
  local ret = {}
  local line = fin:read()
  while 1 do
    motifName = string.match(line,">(%S+)")
    s = ""
    while 1 do
      line=fin:read() 
      if not line then break end
      if string.sub(line,1,1) == '>' then break end
      s=s..line
    end
    ret[#ret+1]={name=motifName, fasta=s}
    if not line then break end
  end
  fin:close()
  return ret
end



function breakup_strings(motifStrs, maxLength,stepsize) 
  if stepsize == nil then
    stepsize = 1
  end
  newMotifs = {}
  nwCounter = 1
  for i=1,#motifStrs do
    motif = motifStrs[i]
    if motif:len() > maxLength then
      for j=1,#motif-maxLength+1,stepsize do
        newMotifs[nwCounter] = motif:sub(j,j+maxLength-1)
        nwCounter = nwCounter + 1
      end
      if maxLength % stepsize ~= 0 then
        newMotifs[nwCounter] = motif:sub(motif:len()-maxLength+1,motif:len())
        nwCounter = nwCounter + 1
      end
    else 
      newMotifs[nwCounter] = motif
      nwCounter = nwCounter + 1
    end
  end  
  return newMotifs
end


-- The motifs cannot be longer than the size of the convolution unit
function insert_motifs(mlp, motifStrs, doInitRandom)

  -- Initialize the mlp to have the motifs as starters
  print("Inserting motifs into convolution unit")
  --print(table.tostring(motifStrs))

  convolayer = mlp:get(1)
  allweights = convolayer.weight

  for j=1,allweights:nDimension() do
    --print("Allweights: Dim", j, "has size", allweights:size(j))
  end
  
  --print('number of motifStrs', #motifStrs)
  --print('motifStrs', table.tostring(motifStrs))
  for i=1,allweights:size(4) do
    --print("i  ->", i)

    -- Get the weights for this motif
    local weights = allweights:select(4,i)
    
    for j=1,weights:nDimension() do
      --print("Weights: Dim", j, "has size", weights:size(j))
    end

    -- Zero out all the weights
    -- We set specific weights to be positive below
    if doInitRandom then
      -- do nothing
    else
      weights:zero()
    end
  end

  

  for i=1,#motifStrs do repeat
    if i > #motifStrs then break end -- continue
    awSize = allweights:size(4)
    if i > awSize then 
      print("Idx is greater than allweights size!")
      print("i:", i)
      print("allweights size:", awSize)
      break -- continue
    end 

    local weights = allweights:select(4,i)
    motif = motifStrs[i]

    if motif:len() > weights:size(1) then
      print("The motif is too long!")
      print("Motif length: ", motif:len()) 
      print("Motif: ", motif) 
      print("Max motif size (same as convosize):", weights:size(1) ) 
      os.exit(1)    
    end

    for j=1,motif:len() do
      char = motif:sub(j,j)
      --print("Char->", char)
      k = bp_to_num(char)
      --print("k   ->", k)
      weights[j][k][1] = 0.5
    end
  until true end

end

