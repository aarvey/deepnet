dofile("dirs.lua")

-- Test fasta processing
dofile("fasta.lua")

for i,dir in ipairs(mapping_dirs) do
  print(i, "Directory: " .. dir)
  dir = prefix .. dir
  fasta = fasta_load(dir .. "/modules_randomflank.fa")
  positions = positions_load(dir .. "/positions.txt")
  for i,tab in ipairs(fasta) do
    s = ""
    for k,v in pairs(tab) do
      if (k=="s") then break end
      s = s .. ", " .. k .. "->" .. v
    end
    --print(s)
  end
  for i,tab in ipairs(positions) do
    s = ""
    for k,v in pairs(tab) do
      s = s .. ", " .. k .. "->" .. v
    end
    print(s)
  end
end

-- Test