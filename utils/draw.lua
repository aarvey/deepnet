require("gfx")
require("lcairo")
require("math")

function drawcircle(cr, x,y, rad, label)
  --print(x,y,rad,0,2*math.pi)
  cr:arc(x,y,rad,0,2*math.pi)
  cr:stroke()
  if label ~= "" then
    print("Label is", label)
    
  end
end

function drawline(cr, x1,y1, x2,y2)
  cr:set_line_width(2.0)
  cr:move_to(x1,y1)
  cr:line_to(x2,y2)
  cr:stroke()
end

function clear(w,cr)
  cr:set_source_rgb(1,1,1)
  cr:paint()
  w:refresh()
end

function drawgraph(w,cr,circles,simmatrix)
  cr:set_source_rgb(0,0,0)
  for i=1,#circles do
    x = circles[i].x
    y = circles[i].y
    drawcircle(cr, x, y, radius, "")
  end
  for i=1,#simmatrix do repeat
    start = simmatrix[i][1]
    fin = simmatrix[i][2]
    if start > #circles or fin > #circles then break end -- continue
    drawline(cr, circles[start].x,circles[start].y,  circles[fin].x,circles[fin].y)
  until true end
  w:refresh()
end


function drawgrid(w, grid)
  w:fromTensor(grid)
  w:refresh()
end




function getgrid(weights)
  print("Weights unnormalized", weights[1][3][1][1],weights[1][3][2][1],weights[1][3][3][1],weights[1][3][4][1]) --,weights[1][1][5][1])
  print("Weights unnormalized", weights[2][3][1][1],weights[2][3][2][1],weights[2][3][3][1],weights[2][3][4][1]) --,weights[1][1][5][1])

  tab = {}
  for i=1,#weights do
    t = torch.Tensor(weights[i]:size(1),weights[i]:size(2))
    -- Normalize the nucleotide weights
    min = 999
    max = -999
    local w = 0
    for j=1,weights[i]:size(1) do
      total = 0
      for k=1,weights[i]:size(2) do
        w = weights[i][j][k][1]
        if min > w then min = w end
      end
      if min > 0 then min = -min end

      for k=1,weights[i]:size(2) do
        w = weights[i][j][k][1]
        t[j][k] = (weights[i][j][k][1] - min)
        if weights[i][j][k][1] <= 0 then
          t[j][k] = 0
        end
        total = total + t[j][k]
        w = t[j][k]
        if max < w then max = w end
      end

      if i==1 and j==1 then
        --print("min,max,total", min, max, total)
      end
      if total < 0 then
        print("TOTAL IS LESS THAN ZERO!!!!")
        print("min,max,total", min, max, total)
      end

      if max <= 0 then max = 1 end
      for k=1,weights[i]:size(2) do
        t[j][k] = t[j][k] / max
      end
    end

    -- Multiply by 255
    --t:mul(255)
    tab[i] = t
  end

  print("Normalized weights to 255", tab[1][3][1],tab[1][3][2],tab[1][3][3],tab[1][3][4]) --,tab[1][1][5])
  print("Normalized weights to 255", tab[2][3][1],tab[2][3][2],tab[2][3][3],tab[2][3][4]) --,tab[1][1][5])


  -- Get the weights into a single tensor so that it can be plotted
  numNucleotides = 4
  numWeights = #weights
  t = torch.Tensor(weights[i]:size(1),numWeights*weights[i]:size(2))
  t:zero()
  print("Num weights: " .. numWeights)
  for i=1,#weights do
    for j=1,weights[i]:size(1) do
      for k=1,weights[i]:size(2) do
        --print(i,j,k)
        t[j][k+((i-1)*weights[i]:size(2))] = tab[i][j][k]
        --t[j][k+((i-1)*numWeights)] = tab[i][j][k]
        --t[j][k+((i-1)*numMotifs*weights[i]:size(2))] = tab[i][j][k]
      end
    end
  end

  -- Scale the tensor up
  ret = torch.Tensor(winsize, winsize)
  for i=1,t:size(1) do
    for j=1,t:size(2) do
      istart = (i-1)*winsize/t:size(1)+1
      iend =  (i-1)*winsize/t:size(1) + winsize/t:size(1) - 1
      jstart = (j-1)*winsize/t:size(2)+1
      jend =  (j-1)*winsize/t:size(2) + winsize/t:size(2) -1
      if iend >= winsize then iend = winsize end
      if jend >= winsize then jend = winsize end
      for k=istart, iend do
        for l=jstart, jend-1 do
          --print("t is:", t)
          --print("t is:", t[i][j])
          --print("ret is:", ret[k][l])
          --print(k, iend, l, jend, i, j)
          ret[k][l] = t[i][j]
          --if i==1 and j<=4 then
          --  ret[k][l] = 1
          --end
        end
        --ret[k][jend] = 1
        --print("j", j)
        if j % numNucleotides == 0 then
          --print("inside j", j)
          ret[k][jend] = 1
        end
      end
    end
  end
  --print(ret)
  return ret
end


