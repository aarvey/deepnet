
--[[

	pargs = read_args( compulsory_args, optional_args )

	parses arg[] array from the command line into a nice table based on compulsory list of items and optional items with defaults and valid parameter lists
	pass in defaults in two arrays - if no command line, just sends these back
	argument strings are converted to same type as defaults
	usage and errors on bad args
	compulsory args appear in order
	optional args are headed by -argname="hello"

	returns: a single table with keys and vals of correct type

eg...
compulsory_args={
  {method="train"	,__valid={"train","test"} },
  {trainfile="trn.mat"	},
  {outfile="good.model"	}
}
optional_args={ 
  {fukit=false		,__valid={true,false}},
  {bye=1		},
  {learning_rate=0.01	},
  {verbose=false	}
}
pargs=read_args (compulsory_args,optional_args)
if pargs.verbose then
  print("gobbledegook")
end
f=io.open(pargs.trainfile)
etc..	

]]--

function string_to_type(s,t)
  if t=="boolean" then
    if s=="true" then
      return true
    elseif s=="false" then
      return false
    elseif tonumber(s) then
      if tonumber(s)==0 then 
        return false
      elseif tonumber(s)==1 then 
        return true 
      end
      print("error: expecting a bool, got:"..s)
      return nil
    end
  elseif t=="number" then
    if tonumber(s) then
      return tonumber(s)
    else
      print("error: expecting a number, got:"..s)
      return nil
    end
  elseif t=="string" then
    return s
  end
end
function cat_options(s,p) 
	s=s..tostring(p.key)
	s=s.."("
	if p.__valid then
	  s=s.."<"
	  for k,q in ipairs(p.__valid) do
		s=s..tostring(q)
		if k<#p.__valid then
			s=s..","
		end
                 end
	  s=s..">:"
	end
	s=s..tostring(p.val)
	s=s..")"
	return s.." "
end

function parse_defaults( def )
  -- copy defauls into arrays of {key=,val=}
  local d={}
  for i,p in ipairs(def) do
    d[i]={}
    for k,p in pairs(def[i]) do 
	if k=="__valid" then
		d[i].__valid=p
	else -- should only be one other
		d[i].key=k
		d[i].val=p
	end
    end 
  end
  return d
end
function override_arg(p,a)
	--p = {key="hello",val=1,__valid={1,2,3}}
	local err=false
	if p.__valid then -- validate (by string)
		local found = false
		for j,q in pairs(p.__valid) do
			astr = tostring(string_to_type(tostring(a),type(p.val)))
			bstr = tostring(string_to_type(tostring(q),type(p.val))) 
			if astr==bstr then 
				found=true break 
			end
		end
		if found==false then 
			print("invalid value for compulsory arg: "..p.key..":"..a)
			err=true 
		end 
	end
	local v = string_to_type(a,type(p.val))
	if v==nil then 
		err=true 
	end

   	return {key=p.key,val=v},err
end

function read_args( defcomp, defopt )

  local oldcomp=parse_defaults(defcomp)
  local newcomp=parse_defaults(defcomp)
  local oldopt =parse_defaults(defopt)
  local newopt =parse_defaults(defopt)

  -- oldcomp ={ 1={key="hello",val=1,__valid={1,2,3}},
  --		2={key="bon"  ,val=true} }

  if not arg then --no command line, just copy the combined defaults back
    local pargs = {}
    for k,p in ipairs(oldcomp) do pargs[p.key]=p.val end
    for k,p in ipairs(oldopt) do pargs[p.key]=p.val end
    return pargs
  end

  -- parse command line into compulsory and optional pairs
  local err=false
  local cargs={}
  local oargs={}
  local skip = false
  for i=1,#arg do
    if not skip then
      c = string.sub(arg[i],1,1)=='-'
      if c then
        p = string.sub(arg[i],2)
        local k,a = string.match(arg[i],"-(%S+)=(%S+)")
        if k and a then
          oargs[k]=a
        else 
	  if arg[i+1] then
            oargs[p]=arg[i+1]
	  else
	    oargs[p]="undefined"
          end
          skip=true
        end
      else  
        cargs[#cargs+1]=arg[i]
      end
    else
      skip=false
    end
  end

  local oerr


  --- compulsory args..
  for i,p in ipairs(oldcomp) do
    if cargs[i] then
      newcomp[i],oerr=override_arg(p,cargs[i])
      if oerr==true then err=true end
    else
      print("error: not enough compulsory args")
      err=true
      break
    end
  end

  if #cargs > #newcomp then
    print("error: too many compulsory args")
    err=true
  end

  ---- optional args..
  for k,p in pairs(oargs) do
    local found=false
    for i,npt in ipairs(newopt) do
        if k == npt.key  then
          found=true
          newopt[i],oerr=override_arg(npt,oargs[k])
          if oerr==true then err=true end
          break
        end
    end
    if found == false then
      print("unrecognised optional arg:"..k.." val:"..p)  
      err=true
    end
  end


  if err==true then
    
    -- error, build acceptable usage, print and quit
    
    local s = "usage: lua "..arg[0].." "
    for i,p in ipairs(oldcomp) do
      s=cat_options(s,p)
    end
    s = s.."[ "
    for i,p in ipairs(oldopt) do
      local tmpk = p.key
      p.key='-'..p.key
      s=cat_options(s,p)
      p.key=tmpk
    end
    s = s.."]"
    print()
    print(s)
    print()
    os.exit(1)
  else

    -- success, 

    local pargs = {}
    for k,p in ipairs(newcomp) do pargs[p.key]=p.val end
    for k,p in ipairs(newopt) do pargs[p.key]=p.val end
    return pargs
  end
end

--[[ something like this goes in calling file...

compulsory_args={
  {method="train"	,__valid={"train","test"} },
  {trainfile="trn.mat"	},
  {outfile="good.model"	}
}
optional_args={ 
  {fukit=false		,__valid={true,false}},
  {bye=1		},
  {learning_rate=0.01	},
  {verbose=false	}
}
pargs=read_args (compulsory_args,optional_args)
if pargs.verbose then
  print("pargs..")
  for k,p in pairs(pargs) do
    print(k,p)
  end
end

]]--
