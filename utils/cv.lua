function train(trainset, layers)
  mlp = nn.Sequential()
  for i=1,#layers do
    mlp:add(layers[i].func)
    print("Added layer " .. layers[i].name)
  end  

  mlp_conv = nn.Sequential()
  mlp_conv:add(layers[1].func)
  mlp_conv:add(layers[2].func)

  --return stochastic_train(trainset, mlp)
  return mytrain(trainset, mlp, mlp_conv)
end

function stochastic_train(trainset, mlp)
  -- Train using the stochastic gradient method
  local dataset = {}
  function dataset:size() return #trainset end
  for i=1,#trainset do
    dataset[i] = {trainset[i].example, trainset[i].label}
  end
  criterion = nn.MSECriterion()
  trainer = nn.StochasticGradient(mlp, criterion)
  trainer.learningRate = pargs.lr
  trainer.maxIteration = pargs.maxIteration
  trainer:train(dataset)
  return mlp
end

-- Taken from stochastic gradient
-- Positive and negative examples taken in separately
function mytrain(trainset, mlp, mlp_conv)
  local iteration = 1
  local learningRate = pargs.lr
  local shuffledIndices = lab.randperm(#trainset)
  local currentLearningRate = learningRate
  local learningRateDecay = 0

  local criterion = nn.MSECriterion()
  print("# StochasticGradient: training")
  while true do
    local currentError = 0
    local convError = 0
    local err = 0
    local input 
    local target
    local motif_labels
    for i = 1,#trainset do
      idx = shuffledIndices[i]
      input = trainset[idx].example
      target = trainset[idx].label

      --motif_labels = trainset[idx].motif_labels
      --err = criterion:forward(mlp_conv_layer:forward(input), motif_labels)
      --mlp_conv_layer:zeroGradParameters()
      --mlp_conv_layer:backward(input, criterion:backward(mlp_conv_layer.output, motif_labels))
      --mlp_conv_layer:updateParameters(currenttLearningRate)
      --convError = convError + err

      err = criterion:forward(mlp:forward(input), target)
      mlp:zeroGradParameters()
      mlp:backward(input, criterion:backward(mlp.output, target))
      mlp:updateParameters(currentLearningRate)
      currentError = currentError + err

      --input_str = negative_example(trainset[idx].s, string.len(trainset[idx].s))
      --input = dna_to_tensor_binary(input_str)
      --target = negative_label
      
      --err = criterion:forward(mlp:forward(input), target)
      --mlp:zeroGradParameters()
      --mlp:backward(input, criterion:backward(mlp.output, target))
      --mlp:updateParameters(currentLearningRate)
      --currentError = currentError + err
    end
    currentError = currentError / (#trainset*2)
    print("# current error = " .. currentError)
    iteration = iteration + 1
    currentLearningRate = learningRate/(1+iteration*learningRateDecay)
    if pargs.maxIteration > 0 and iteration > pargs.maxIteration then
      print("# StochasticGradient: you have reached the maximum number of iterations")
      break
    end
   end

  return mlp
end




function do_loov(trainset, mlp, trainfunc, errorfunc)
  local trainset_copy = {}
  for i=1,#trainset do
    trainset_copy[i] = trainset[i]
  end
  for i=1,#trainset do
    -- Set up the training set less one example
    local newtrainset = {}
    for j=1,#trainset_copy do
      if j==i then
        -- do nothing
      elseif j>i then
        newtrainset[j-1] = trainset_copy[j]
      elseif j<i then
        newtrainset[j] = trainset_copy[j]
      end
    end

    -- Train mlp on the training set
    mlp, layers = getmlp(newtrainset, numMotifs, motifSize)
    print(mlp)
    mlp = trainfunc(newtrainset, layers)
    --mlp = trainfunc(newtrainset, mlp)
    trainErrorStr = errorfunc(mlp, newtrainset)
    local testset = {}
    testset[1] = trainset_copy[i]
    testErrorStr = errorfunc(mlp, testset)
    print(i, trainErrorStr, testErrorStr)
    --write_mlp(mlp, layers)
  end
end


function do_seq_loov(fasta, position, trainset, mlp)
  local trainset_copy = {}
  for i=1,#trainset do
    trainset_copy[i] = trainset[i]
  end
  for i=1,#trainset do
    -- Set up the training set less one example
    local newtrainset = {}
    for j=1,#trainset_copy do
      if j==i then
        -- do nothing
      elseif j>i then
        newtrainset[j-1] = trainset_copy[j]
      elseif j<i then
        newtrainset[j] = trainset_copy[j]
      end
    end

    -- Train mlp on the training set
    mlp, layers = getmlp(newtrainset)
    train(newtrainset, layers)
    trainError = error_rate(mlp, newtrainset)

    print("---- Start LOOV " .. i .. "----")
    local testset = getseqtestset({fasta[i]}, {position[i]})
    testError = seq_error_rate(mlp, testset)
    print(i, "trainError",trainError, "testError", testError)
    write_mlp(mlp, layers)
    print("---- End LOOV " .. i .. "----")
  end
end



function twoclass_error(mlp, testset)
  local criterr = 0
  local criterion = nn.AbsCriterion()
  local perr = 0
  local nerr = 0
  local nump = 0
  local numn = 0
  for i=1,#testset do
    --print('Testset value', i, testset[i].s, testset[i].label)
    input = testset[i].example
    target = testset[i].label
    pred = mlp:forward(input)
    err = criterion:forward(pred, target)
    criterr = criterr + err
    --print(target[1], target[2], target:nDimension(), target:size()[1])
    --print(pve[1], pve[2])
    --print(nve[1], nve[2])
    if target[1] == pve[1] and target[2] == pve[2] then
      --print(pred, pve, nve)
      if not iscloser(pred, pve, nve) then 
        perr = perr + 1
      end
      nump = nump + 1
    elseif target[1] == nve[1] and target[2] == nve[2] then
      --print(pred, nve, pve)
      if not iscloser(pred, nve, pve) then 
        nerr = nerr + 1
      end
      numn = numn + 1
    else
      print("Can't recognize target!")
    end
  end
  --print('errs: ', perr, nerr)
  --print('nums: ', nump, numn)
  if nump + numn ~= #testset then
    print("Number of examples don't add up!")
  end
  totalerr = (perr+nerr) / (nump + numn)
  criterr = criterr/#testset
  perr = nump>0 and perr/nump or 0
  nerr = numn>0 and nerr/numn or 0
  s = " " .. perr  .. " " .. nerr .. " " .. totalerr
  return s
end



