function motifscan(str, motif)
  local mlen = #motif
  local ret = {}
  if mlen < 6 then
    for i=1,#str do
      ret[i]="0"
    end
    return ret
  end
  local label = "0"
  local last_found_idx = -mlen-10
  for i=1,#str do
    label = "0"
    if string.sub(str,i,i+mlen-1) == motif then 
      --print(i, str, motif)
      last_found_idx = i
    end
    if last_found_idx > i - mlen then
      label = "1"
    end 
    ret[i] = label
  end
  return ret
end

function merge_motifs(motif1, motif2) 
  if not #motif1 == #motif2 then
    print("Motifs are different lengths!  Cannot merge!")
    os.exit(1)
  end
  for i=1,#motif1 do
    if motif1[i]=="1" or motif2[i]=="1" then
      motif1[i] = "1"
    end
  end
  return motif1
end


function find_motifs(trainset, motifs) 
  for j=1,#motifs do
    print(motifs[j].name, motifs[j].s)
  end
  for i=1,#trainset do
    print(trainset[i].name, trainset[i].s)
  end

  local motif_labels = {}
  for i=1,#trainset do
    print("Scanning " .. trainset[i].name)
    local motif_label
    motif_labels[i] = {}
    for j=1,string.len(trainset[i].s) do
      motif_labels[i][j] = "0"
    end
    for j=1,#motifs do
      --print(j, motifs[j].s)
      motif_label = motifscan(trainset[i].s, motifs[j].s)
      motif_labels[i] = merge_motifs(motif_labels[i], motif_label)
      --if string.find(motif_labels[i][j], "1") then 
      --  print(motif_labels[i][j])
      --end
    end
    motif_labels[i] = table.concat(motif_labels[i])
    print(trainset[i].s)
    print(motif_labels[i])
  end
  return motif_labels
end


function test_motifs()
  require("torch")
  dofile("utils/dirs.lua")
  dofile("utils/fasta.lua")
  dofile("dataset.lua")

  pargs = {}
  pargs.negMultiplier = 1

  mapping = "mapping1.neuroectoderm"
  dir = prefix .. mapping
  local fasta = fasta_load(dir .. "/modules_randomflank.fa")
  local positions = positions_load(dir .. "/positions.txt")

  local trainset = gettrainset(fasta, positions)
  local motifs = fasta_load("../redfly/redfly_tfbs.fasta")
  local motif_labels = find_motifs(trainset, motifs)
end

--test_motifs()