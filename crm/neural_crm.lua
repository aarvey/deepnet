-- Get the system modules
require("torch")
require("nn")
require("math")
require("os")
require("lcairo")
--require("gfx")

-- Get the utilities for read, parsing, etc
dofile("../utils/dirs.lua")
dofile("../utils/base.lua")
dofile("../utils/args.lua")
dofile("../utils/fasta.lua")
dofile("../utils/blosum.lua")
dofile("../utils/membership.lua")
dofile("../utils/model.lua")
dofile("../utils/cv.lua")

-- Get the functions for neural net construction/execution
dofile("mlp.lua")
dofile("dataset.lua")
dofile("motifs.lua")


--------------------------------------------------
------- Take care of command line params ---------
--------------------------------------------------

compargs= {
  {mode	  	 ="train",__valid={"train","test"}},
  {mapping       = "mapping1.amnioserosa" },
  {maxIteration  = 20 },
  {negMultiplier = 3 },
  {cvtype = "natural", __valid={"segmented","natural"} },
}
optargs={
  {method	= "tpmax",__valid={"tpmean","tpmax"}},
  {HUs		= 10	},
  {numMotifs	= 10	},
  {lr		= 0.01	},
  {startEpoch	= 0     },
  {features	= "binary",__valid={"binary","blosum"}},
  {ext		= ""},	--model fname extension
  {testModel 	= "models/a.model"},
}
pargs=read_args(compargs,optargs)
for k,p in pairs(pargs) do print(k,p) end

--------------------------------------------------
----- Load in the fasta/position files -----------
--------------------------------------------------

local mappingsets = {}
local fastas = {}
local positions = {}
local mappingindex = 0
for i,dir in ipairs(mapping_dirs) do
  --print(i, "Reading directory: " .. dir)
  if pargs.mapping==dir then
    print(i, "Using directory: " .. dir)
    mappingindex = i
  end
  local dir = prefix .. dir
  fastas[i] = fasta_load(dir .. "/modules_randomflank.fa")
  positions[i] = positions_load(dir .. "/positions.txt")
  for i,tab in ipairs(fastas[i]) do
    local s = ""
    for k,v in pairs(tab) do
      if (k=="s") then 
	s = s .. ", " .. string.sub(v,1,10) .. "..." 
      else
        s = s .. ", " .. k .. "->" .. v
      end
    end
    --print("fasta: " .. s)
  end
  for i,tab in ipairs(positions[i]) do
    local s = ""
    for k,v in pairs(tab) do
      s = s .. ", " .. k .. "->" .. v
    end
    --print("positions: " .. s)
  end
  --print("Read in :", #fastas[i], #positions[i], "examples")
end


------------------------------------------------------
--- Choose which mapping we will use to learn from ---
------------------------------------------------------

if mappingindex <= 0 then
  print("You need to specify a valid mapping")
  print("Mapping given is '" .. pargs.mapping .. "'")
  os.exit(0)
end
local fasta = fastas[mappingindex]
local position = positions[mappingindex]

if not (#fasta == #position) then
  print("Length of fasta:", #fasta)
  print("Length of position:", #position)
  error("       Length of position and sequences do not match up!")
end

-- >(CCAGGTTG,0,-1,-1); (CGATTAGC,0,-1,-1); (AAGCAATA,0,-1,-1); (TATAATGC,0,-1,-1); (ACATGCTA,0,-1,-1); 
-- AAGGCTCCACGTCTCGAGCCAATAAGGCCCATACTATCGATGTTGTTCGAGATCGGTAATGCAGCTTGCCAGACGATGGCTCTCTTAACTGCAGGAAATGTGCA
convert_format = false
if convert_format then
  for i=1,#fasta do 
    for k,v in pairs(fasta[i]) do
      --print(i,k,'->',v)
    end
    for k,v in pairs(position[i]) do
      --print(i,k,'->',v)
    end
    print('>(FAKEMOTIF,1,1,9); (FAKEMOTIF,1,1,9); (FAKEMOTIF,1,1,9); (FAKEMOTIF,1,1,9); (FAKEMOTIF,1,1,9); ')
    --print(position[i].seq_start, position[i].seq_finish)
    crmregion = fasta[i].s:sub(position[i].seq_start, position[i].seq_finish)
    --print(crmregion:len())
    print(crmregion)
  end
  os.exit()
end

------------------------------------------
------- Get trainset and run on it -------
------------------------------------------

local trainset = gettrainset(fasta, position)
--local motifs = fasta_load("../redfly/redfly_tfbs.fasta")
--motif_labels = find_motifs(trainset, motifs)

if pargs.cvtype == "segmented" then
  do_loov(trainset, layers, train, error_rate)
elseif pargs.cvtype == "natural" then 
  do_seq_loov(fasta, position, trainset, mlp)
end

os.exit()


























































-- mlp, layers = getmlp(trainset)
-- train(trainset, mlp)
-- trainError = error_rate(mlp, trainset)
-- print("trainError",trainError)

-- local testset = getseqtestset(fasta, position)
-- testError = seq_error_rate(mlp, testset)
-- print("testError", testError)

os.exit()

a = getmlp()
mlp = a[1]
layers = a[2]
intermediate_mlps = get_intermediate_mlps(layers)





--mlp:add( nn.SpatialConvolution(1, pargs.HUs, motif_size, xtrn_example:size(2)))
--mlp:add(nn.Mean(1))
--mlp:add(nn.Reshape(pargs.HUs))

--[[
mlp:add( nn.SpatialConvolution(1, pargs.HUs, crm_size, xtrn_example:size(2)))
mlp:add( nn.Mean(1))
mlp:add( nn.Reshape(pargs.HUs))
]]--

--mlp:add( nn.Linear(pargs.HUs,num_classes))

--mlp:add(nn.TemporalConvolution(1, pargs.HUs, motif_size))
--------------------------------------------------------
---------------- Train the Neural Network --------------
--------------------------------------------------------



function get_pred_dim(mlp, example)
  pred = mlp:forward(trainset[1].example)
  s = "pred dim: " .. pred:size(1)
  for i=2,pred:nDimension() do
    s = s .. "x" .. pred:size(i)
  end
  print(s)  
end

get_pred_dim(mlp, trainset[1].example)
for i=1,#intermediate_mlps do
  get_pred_dim(intermediate_mlps[i], trainset[1].example)
end

os.exit()



for i=1,#trainset do
  pred = conv_layer_mlp:forward(trainset[i].example)
  print(pred)
  pred = conv_transfer_mlp:forward(trainset[i].example)
  print(pred)
end


os.exit()

-- Train manually
local maxIteration = pargs.maxIteration
local epoch = 0
local max_iteration = 100
for epoch = epoch+1,max_iteration do
  model_name = make_filename(epoch,".model")
  error_name = make_filename(epoch,".error")
  msError=0; 
  for i=1,#trainset do
    x = trainset[i].example
    y = trainset[i].label
    --print(x:nDimension())
    --print(x:size(1), x:size(2))
    pred = mlp:forward(x)

    --local example = torch.Array(); example:add(x); example:add(y) 
    local err=  criterion:forward(pred, y); 
    msError = msError + err;
    mlp:zeroGradParameters();
    mlp:backward(x, criterion:backward(pred, y)); 

    mlp:updateParameters(learningRate);
--    for i=1,mlp:size() do
--      sz = mlp:get(i).output:size(1)
--      mlp:get(i):updateParameters(trainer.learningRate/sz)
--    end
    if (i%1000)==0 then -- output MSE every 10000 examples
      collectgarbage()
      print(model_name.." example:"..j.."/"..#xtrns.."\tcurMSE:",msError/j) 
    end
  end -- for j

  --save_model(model_name); 
  mseError = msError/#trainset
  --testError = error_rate(mlp,ytsts)
  trainError = error_rate(mlp, trainset)
  --f=io.open(error_name,"w+")
  --f:write(mseError.."\t"..testError.."\t"..trainError.."\n")
  print("mse_err",mseError,"train_err",trainError,"test_err",testError)
  --f:close()
  --print("[saved model]:"..model_name)
end -- for epoch



------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------
------------------------------------------------------------------------





criterion = nn.MSECriterion()
trainer = {};
trainer.learningRate = pargs.lr
trainer.maxIteration = pargs.maxIteration
currentExample=0;
epoch = 0

motif_size = 6
expected_num_motifs = 15
crm_window = 500
num_classes=2

layers = {}
layers[1] = nn.TemporalConvolution(1, expected_num_motifs, motif_size)
layers[2] = nn.Max(1)
layers[3] = nn.TemporalConvolution(1, crm_window, motif_size)
layers[4] = nn.Mean(1)
layers[5] = nn.Reshape(pargs.HUs) 
layers[6] = nn.Linear(pargs.HUs,num_classes)

mlp=nn.Sequential()
mlp:add(layers[1])
mlp:add(layers[2])
mlp:add(layers[3])
mlp:add(layers[4])
mlp:add(layers[5])
mlp:add(layers[6])

------------------------------------------------

for epoch = epoch+1,trainer.maxIteration do
  model_name = make_filename(epoch,".model")
  error_name = make_filename(epoch,".error")
  msError=0; 
  for j = 1,#xtrns do
    ex=math.floor(math.random()*#xtrns)+1
    y= ytrns[ex]
    x= protein_to_tensor(xtrns[ex])
    pred=mlp:forward(x);

    example = torch.Array(); example:add(x); example:add(y) 
    local err=  criterion:forward(pred, example); 
    msError = msError + err;
    currentExample = currentExample + 1;
    mlp:zeroGradParameters();
    mlp:backward(criterion:backward(mlp.output, example), x); 

    mlp:updateParameters(trainer.learningRate);
--    for i=1,mlp:size() do
--        sz = mlp:get(i).output:size(1)
--        mlp:get(i):updateParameters(trainer.learningRate/sz)
--    end
    
    if (currentExample % 10000) ==0 then -- output MSE every 10000 examples
      collectgarbage()
      print(model_name.." example:"..j.."/"..#xtrns.."\tcurMSE:",msError/j) 
    end
  end -- for j

  save_model(model_name); 
  mseError = msError/#xtrns
  testError = error_rate(xtsts,ytsts)
  trainError = error_rate(xtrns,ytrns)
  f=io.open(error_name,"w+")
  f:write(mseError.."\t"..testError.."\t"..trainError.."\n")
  print("mse_err",mseError,"train_err",trainError,"test_err",testError)
  f:close()
  print("[saved model]:"..model_name)
end -- for epoch
