-- Global variables

minimum_length = 0

positive_label = torch.Tensor(2)
positive_label[1]=1
positive_label[2]=0

negative_label = torch.Tensor(2)
negative_label[1]=0
negative_label[2]=1


-- Global random seed
math.randomseed(137)
chars = {"A", "C", "G", "T"}
function random_string(len)
  local result = {}
  for i=1,len do
    result[i] = chars[math.random(1,#chars)]
  end
  return table.concat(result)
end


function randperm(str)
  local result = {}
  local len = string.len(str)
  for i=1,len do
    r = math.random(1,len)    
    result[i] = string.sub(str,r,r)
  end
  return table.concat(result)
end


function negative_example(sidestr, len)
  while string.len(sidestr) < minimum_length do 
    sidestr = sidestr .. sidestr
  end
  str = randperm(sidestr)

  if (minimum_length > 0) then 
    str = string.sub(str, 1, minimum_length)
  else 
    len = len + math.random(-len/5, len/5)
    str = string.sub(str, 1, len)
  end
  return str
end


function gettrainset(fasta, position)
  local trainset = {}

  -- Put in all the positive examples
  local pve=torch.Tensor(2)
  pve[1]=1
  pve[2]=0
  for i=1,#fasta do repeat
    local crm_region = {start=position[i].seq_start, finish=position[i].seq_finish}
    local str = string.sub(fasta[i].s,crm_region.start, crm_region.finish)

    if (string.len(str) < minimum_length) then
      lstr = string.sub(str,1,crm_region.start-10)
      rstr = string.sub(str,crm_region.finish+10,string.len(str))
      sidestr = lstr .. rstr
      while string.len(sidestr) < minimum_length do 
        sidestr = sidestr .. lstr .. rstr
      end
      randstr = randperm(sidestr)
      str = str .. string.sub(randstr, 1, minimum_length-string.len(str))
    end

    trainset[#trainset+1] = {}
    trainset[#trainset].example = dna_to_tensor_binary(str)
    trainset[#trainset].s = str
    trainset[#trainset].name = fasta[i].name
    trainset[#trainset].label = pve
  until true end

  function trainset:possize() return #trainset end

  -- Negative example data
  local num_negative_examples = #trainset * pargs.negMultiplier
  local nve=torch.Tensor(2)
  nve[1]=0
  nve[2]=1

  -- Put in negative examples that are taken from Sinha's random sequence
  local number_of_positives = #trainset
  print("pargs neg multiplier  " .. pargs.negMultiplier)
  for j=1,#trainset do
    for k=1,pargs.negMultiplier do
      i = #trainset + 1
      trainset[i] = {}

      idx = math.random(1,number_of_positives)
      local crm_region = {start=position[idx].seq_start, finish=position[idx].seq_finish}
      lstr = string.sub(fasta[idx].s,1,crm_region.start-10)
      rstr = string.sub(fasta[idx].s,crm_region.finish+10,string.len(fasta[idx].s))
      sidestr = lstr .. rstr

      str = negative_example(sidestr, crm_region.finish - crm_region.start)

      trainset[i].example = dna_to_tensor_binary(str)
      trainset[i].s = str
      trainset[i].name = fasta[idx].name .. 'randomized_negative' .. i
      trainset[i].label = nve
    end
  end

  for i=1,#trainset do
    s = "" .. i
    for k,v in pairs(trainset[i]) do repeat
      if k=="example" then
        break
      elseif k=="label" then
        s = s .. ",   "  .. k .. "->" .. v[1]
      elseif k=="s" then
        s = s .. ",   "  .. k .. "->" .. string.sub(v,1,10)
      else
        s = s .. ",   "  .. k .. "->" .. v
      end
    until true end
    print(s)
  end

  function trainset:size() return #trainset end

  return trainset
end


--   -- Put in negative examples that are completely random
--   for j=1,num_negative_examples do
--     i = #trainset + 1
--     trainset[i] = {}
--     local random_string_length = 250 + math.random(1,2000)
--     local s = random_string(random_string_length)
--     trainset[i].example = dna_to_tensor_binary(s)
--     trainset[i].name = "randomstring" .. i
--     trainset[i].label = nve
--   end

--   --[[ Print out trainset
--   for i=1,#trainset do
--     s = "" .. i
--     for k,v in pairs(trainset[i]) do repeat
--       if k=="example" then
--         break
--       elseif k=="label" then
--         s = s .. ",   "  .. k .. "->" .. v[1]
--       else
--         s = s .. ",   "  .. k .. "->" .. v
--       end
--     until true end
--     --print(s)
--   end
--   --]]

--   return trainset
-- end


function getseqtestset(fasta, position)
  local testset = {}
  local pve=torch.Tensor(2)
  pve[1]=1
  pve[2]=0
  for i=1,#fasta do
    testset[i] = {}
    local crm_region = {start=position[i].seq_start, finish=position[i].seq_finish}
    testset[i].example = dna_to_tensor_binary(fasta[i].s)
    testset[i].name = fasta[i].name
    testset[i].crm_region = crm_region
    testset[i].fasta = fasta[i].s
    testset[i].label = pve
  end
  return testset
end


