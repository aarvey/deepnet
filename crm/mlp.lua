function getmlp(trainset)
  local xtrn_example = trainset[1].example
  local num_classes=2
  local motif_size = 20
  local crm_size = 50
  local layers = {}
  -- First layer is convolution
  print(xtrn_example:size(1))
  print(xtrn_example:size(2))
  print(xtrn_example:size(3))

  local testnet = nn.Sequential()

  layers[#layers+1] = {}
  layers[#layers].func = nn.SpatialConvolution(1, pargs.HUs, motif_size, xtrn_example:size(2))
  layers[#layers].name = "SC" .. pargs.HUs .. "_KW" .. motif_size
  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  print(x:size(1))
  print(x:size(2))
  print(x:size(3))


--[[
  --layers[#layers+1] = nn.SpatialConvolution(pargs.numMotifs, pargs.HUs, crm_size, 1)
  --testnet:add(layers[#layers])
  --x = testnet:forward(xtrn_example)
  --print(x:size(1))
  --print(x:size(2))
  --print(x:size(3))


  --layers[#layers+1] = nn.Max(3)
  --testnet:add(layers[#layers])
  --x = testnet:forward(xtrn_example)
  --print(x:size(1))
  --print(x:size(2))

  --layers[#layers+1] = nn.TemporalConvolution(1, pargs.HUs, crm_size)
  --testnet:add(layers[#layers])
  --x = testnet:forward(xtrn_example)
  --print(x:size(1))
  --print(x:size(2))

  -- Second and Third layers are for filling in height & width
  width_filler_mlp = nn.Concat(1)
  width_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  width_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  layers[#layers+1] = width_filler_mlp
  height_filler_mlp = nn.Concat(2)
  --height_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  --height_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  height_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  --height_filler_mlp:add(nn.SpatialConvolution(pargs.HUs,pargs.HUs,1,1))
  layers[#layers+1] = height_filler_mlp
  -- Fourth layer is subsampling
  --layers[#layers+1] = nn.SpatialSubSampling(pargs.HUs, 300, 1)
  --layers[#layers+1] = nn.Mean(2)
  --layers[#layers+1] = nn.SpatialConvolution(pargs.HUs, pargs.HUs, 50, 1)
  --layers[#layers+1] = nn.SoftMax()
  layers[#layers+1] = nn.Mean(2)
  ]]--

  layers[#layers+1] = {}
  layers[#layers].func = nn.Max(1)
  layers[#layers].name = "Max"
  --layers[#layers+1] = {}
  --layers[#layers].func = nn.Mean(1)
  --layers[#layers].name = "Mean"

  layers[#layers+1] = {}
  layers[#layers].func = nn.Reshape(pargs.HUs)
  layers[#layers].name = "Reshape"

  layers[#layers+1] = {}
  layers[#layers].func = nn.Linear(pargs.HUs,pargs.HUs)
  layers[#layers].name = "Linear"

  layers[#layers+1] = {}
  layers[#layers].func = nn.SoftMax()
  layers[#layers].name = "SoftMax"

  --layers[#layers+1] = {}
  --layers[#layers].func = nn.HardTanh()
  --layers[#layers].name = "HardTanh"

  layers[#layers+1] = {}
  layers[#layers].func = nn.Linear(pargs.HUs,num_classes)
  layers[#layers].name = "Linear"

  mlp=nn.Sequential()
  for i=1,#layers do
    mlp:add(layers[i].func)
    print("Added layer " .. layers[i].name)
  end

  -- Initialize the mlp to have no motifs as starters
  convolayer = mlp:get(1)
  allweights = convolayer.weight
  for i=1,pargs.HUs do
    weights = allweights:select(4,i)
    weights:zero()
  end

  return mlp, layers
end


function write_mlp(mlp, layers) 
  local fname = "NeuralCRM_"
  for i=1,#layers do
    fname = fname .. "_" .. layers[i].name
  end
  local r = tostring(math.random()*9999999)
  fname = fname .. r .. ".mlp"

  fname = "./models/" .. fname

  f = torch.DiskFile(fname, "w")
  f:writeObject(mlp) 
end




function get_intermediate_mlps(layers)
  inter_mlps = {}
  for i=1,#layers do
    inter_mlps[i] = nn.Sequential()
    for j=1,i do
      inter_mlps[i]:add(layers[j].func)
    end
  end
  --[[
  conv_layer_mlp=nn.Sequential()
  conv_layer_mlp:add(layers[1].func)

  conv_transfer_mlp=nn.Sequential()
  conv_transfer_mlp:add(layers[1].func)
  conv_transfer_mlp:add(layers[2].func)

  conv_filler_mlp=nn.Sequential()
  conv_filler_mlp:add(layers[1].func)
  conv_filler_mlp:add(layers[2].func)
  conv_filler_mlp:add(layers[3].func)

  conv_fillermean_mlp=nn.Sequential()
  conv_fillermean_mlp:add(layers[1].func)
  conv_fillermean_mlp:add(layers[2].func)
  conv_fillermean_mlp:add(layers[3].func)
  conv_fillermean_mlp:add(layers[4].func)
  ]]--
  return inter_mlps
end



function error_rate(mlp, set)
  local ncorrect=0
  for i=1,#set do
    x = set[i].example
    y = set[i].label
    pred=mlp:forward(x)
    if pred[1] > pred[2] and y[1]>0 then
      ncorrect=ncorrect+1
        --[[
      print("correct",
            string.format("%.04f",pred[1]),
            string.format("%.04f",pred[2]),
            string.format("%.04f",y[1]),
            string.format("%.04f",y[2]))
        ]]--
    elseif pred[2] > pred[1] and y[2]>0 then
      ncorrect=ncorrect+1
        --[[
      print("correct",
            string.format("%.04f",pred[1]),
            string.format("%.04f",pred[2]),
            string.format("%.04f",y[1]),
            string.format("%.04f",y[2]))
        ]]--
    else
        --[[
       print("wrong",
             string.format("%.04f",pred[1]),
             string.format("%.04f",pred[2]),
             string.format("%.04f",y[1]),
             string.format("%.04f",y[2]))
        ]]--
    end
  end
  return " " .. (#set-ncorrect)/#set
end

-- function seq_pred(mlp, x, predstart, predend, lastchanged)
--   str = string.sub(x,predstart,predend)
--   local pred = mlp:forward(str)
--   if pred[1] > pred[2] then
--     if lastchanged=="left" then
--       predend = predend - ((predend - predstart) / 10.0)
--     elseif lastchanged=="right" then
--       predstart = predstart + ((predend - predstart) / 10.0)
--     else
--       print("Need to specify left or right was last changed")
--       os.exit(2)
--     end
--     predstart, predend = seq_pred(mlp, x, predstart, predend)
--   end
--   return predstart, predend
-- end

function make_seqset(str) 
  local substrings = {}
  local window_size = 100
  local slen = string.len(str)
  local i = 1
  for sublen=500,2500,250 do
    for start=1,slen,window_size do repeat
      finish = math.min(start+sublen, slen)
      if finish - start < sublen then 
        break
      end
      substr = string.sub(str, start, finish)
      substrings[i] = {}
      substrings[i].str = substr
      substrings[i].example = dna_to_tensor_binary(substr)
      substrings[i].start = start
      substrings[i].finish = finish
      substrings[i].sublen = sublen
      i = i + 1
    until true end
  end
  return substrings
end

function sign(d) 
  if d < 0 then return -1 end
  return 1
end

function seq_pred(mlp, str)
  substrings = make_seqset(str)
  xval = {}
  yval = {}
  for i=1,#substrings do
    local x = substrings[i].example
    local pred = mlp:forward(x)
    start, finish = substrings[i].start, substrings[i].finish
    print(start, finish, pred[1], pred[2])
    sublen = substrings[i].sublen
    xval[i] = start
    yval[i] = pred[1]
  end
  --w=gfx.Window()
  --p1=w:plot(xval,yval,'bx')
  --writePNG("hello.png")  
  local pred = mlp:forward(dna_to_tensor_binary(str))
  return pred
end

function seq_error_rate(mlp, set)
  local ncorrect=0
  local str = set[1].fasta
  for i=1,#set do
    str = set[i].fasta
    x = set[i].example
    y = set[i].label
    --pred=seq_pred(mlp, str, 1, string.len(x), "right")
    print("Sequence Name:", set[i].name)
    print("Correct region:", set[i].crm_region.start, set[i].crm_region.finish)
    pred_region=seq_pred(mlp, str)

    if pred[1] > pred[2] and y[1]>0 then
      ncorrect=ncorrect+1
    elseif pred[2] > pred[1] and y[2]>0 then
      ncorrect=ncorrect+1
    else
    end
  end
  return (#set-ncorrect)/#set
end

