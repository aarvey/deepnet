-- Get the system modules
require("torch")
require("nn")
require("math")
require("os")
require("lcairo")
--require("gfx")

-- Get the utilities for read, parsing, etc
--dofile("../utils/cv.lua")
dofile("../utils/dirs.lua")
dofile("../utils/base.lua")
dofile("../utils/args.lua")
dofile("../utils/fasta.lua")
dofile("../utils/blosum.lua")
dofile("../utils/membership.lua")
dofile("../utils/model.lua")
dofile("../utils/draw.lua")
dofile("../utils/print.lua")


-- Get the aux functions
dofile("./mlp.lua")

runNumber = arg[1]
trainfile = arg[2]
testfile = arg[3]
maxIteration = tonumber(arg[4])
HUs = tonumber(arg[5])
numMotifs = tonumber(arg[6])
motifSize = tonumber(arg[7])
motifFname = arg[8]

usemotiflabels = false

math.randomseed(runNumber)

function randperm(str)
  local result = {}
  local len = string.len(str)
  for i=1,len do
    r = math.random(1,len)    
    result[i] = string.sub(str,r,r)
  end
  return table.concat(result)
end


function negative_example(sidestr, len)
  while string.len(sidestr) < len + (len/5) do 
    sidestr = sidestr .. sidestr
  end
  str = randperm(sidestr)

  len = len 
  str = string.sub(str, 1, len)
  return str
end



--------------------------------------------------
------- Take care of command line params ---------
--------------------------------------------------

fasta, motifStrs = motif_fasta_load(trainfile)
--testfasta, motifStrs = motif_fasta_load(testfile)

motifTab = read_motifs(motifFname)
motifStrs = {}
for i=1,#motifTab do
  --motifStrs[i] = motifTab[i].fasta
end

------------------------------------------
------- Get trainset  -------
------------------------------------------

local pve
local nve
if usemotiflabels then 
  nve = torch.Tensor(numMotifs)
  for i=1,numMotifs do
    nve[i] = 0
  end
  pve = torch.Tensor(numMotifs)
  for i=1,numMotifs do
    nve[i] = 1
  end
else 
  pve = torch.Tensor(2)
  pve[1] = 1
  pve[2] = 0

  nve = torch.Tensor(2)
  nve[1] = 0
  nve[2] = 1
end

print("size of labels:", pve:size()[1], nve:size()[1])

function fasta_to_dataset(fasta, position)
  local trainset = {}
  for i=1,#fasta do

    -- Get the example
    bigstr = fasta[i].s

    strs = breakup_strings({bigstr}, 1000000000, 50)
    print(table.tostring(strs))
    for j=1,#strs do
      str = strs[j]
      trainset[#trainset+1] = {}
      trainset[#trainset].example = dna_to_tensor_binary(str)
      trainset[#trainset].s = str
      trainset[#trainset].bigs = bigstr
      trainset[#trainset].bigexample = dna_to_tensor_binary(bigstr)

      -- Get the label
      if usemotiflabels then
        label = torch.Tensor(numMotifs)
        numpresent = 0
        for j=1,numMotifs do
          label[j] = fasta[i].ispresent[j]
          if label[j] > 0 then numpresent = numpresent + 1 end
        end
        --print("using raw label")
        trainset[#trainset].label = label
      else
        print(table.tostring(fasta[i].ispresent))
        numpresent = 0
        for j=1,#fasta[i].ispresent do
          numpresent = numpresent + tonumber(fasta[i].ispresent[j])
        end
        ispve = numpresent > 0
        print("ispve:", ispve)
        if ispve then
          print("using pos label")
          trainset[#trainset].label = pve
        else 
          print("using neg label")
          trainset[#trainset].label = nve
        end
      end
    end
  end
  function trainset:possize() return #trainset end
  return trainset
end
local trainset = fasta_to_dataset(fasta)
--local testset = fasta_to_dataset(testfasta)
print("Size of trainset is " .. #trainset)
--print("Size of testset is " .. #testset)
print("Size of first train example is:" .. trainset[1].s:len())

make_negative_examples = false
if make_negative_examples then
  for i=1,#trainset do
    print('>(CCAGGTTG,0,-1,-1); (CGATTAGC,0,-1,-1); (AAGCAATA,0,-1,-1); (TATAATGC,0,-1,-1); (ACATGCTA,0,-1,-1); ')
    x = negative_example(trainset[i].s,string.len(trainset[i].s))
    print(x)
  end
  os.exit(0)
end

------------------------------------------
-------------- Run on trainset -----------
------------------------------------------

mlp, layers = getmlp(trainset,HUs,motifSize,motifStrs)

 
winsize = 400
--w = lcairo.Window(winsize,winsize)
--cr = lcairo.Cairo(w)
img = lcairo.Image(winsize,winsize)
cr = lcairo.Cairo(img)


function iscloser(pred, lab1, lab2) 
  dist1 = (pred[1] - lab1[1])^2 + (pred[2] - lab1[2])^2
  dist2 = (pred[1] - lab2[1])^2 + (pred[2] - lab2[2])^2
  print("Labels 1,2 are", lab1, lab2)
  print("dist from label 1", dist1)
  print("dist from label 2", dist2)
  if dist1 < dist2 then
    return true
  end
  return false

  --d = pred:nDimension()
  --for i=1,d do 
  --end
end

function mlperror(mlp, testset)
  local criterr = 0
  local criterion = nn.AbsCriterion()
  local perr = 0
  local nerr = 0
  local nump = 0
  local numn = 0
  for i=1,#testset do
    --print('Testset value', i, testset[i].s, testset[i].label)
    input = testset[i].example
    target = testset[i].label
    pred = mlp:forward(input)
    err = criterion:forward(pred, target)
    criterr = criterr + err
    print('Target is', target[1], target[2], target:nDimension(), target:size()[1])
    print('PVE:', pve[1], pve[2])
    print('NVE:', nve[1], nve[2])
    print('Pred is:', pred)
    if target[1] == pve[1] and target[2] == pve[2] then
      --print(pred, pve, nve)
      if not iscloser(pred, pve, nve) then 
        perr = perr + 1
      end
      nump = nump + 1
    elseif target[1] == nve[1] and target[2] == nve[2] then
      --print(pred, nve, pve)
      if not iscloser(pred, nve, pve) then 
        nerr = nerr + 1
      end
      numn = numn + 1
    else
      print("Can't recognize target!")
    end
  end
  --print('errs: ', perr, nerr)
  --print('nums: ', nump, numn)
  if nump + numn ~= #testset then
    print("Number of examples don't add up!")
  end
  totalerr = (perr+nerr) / (nump + numn)
  criterr = criterr/#testset
  perr = nump>0 and perr/nump or 0
  nerr = numn>0 and nerr/numn or 0
  s = " " .. perr  .. " " .. nerr .. " " .. totalerr
  return s
end


function updatemlp(mlp, criterion, input, target, currentLearningRate)
  criterion = nn.MSECriterion()
  err = criterion:forward(mlp:forward(input), target)
  if err > 1 then
    currentLearningRate = currentLearningRate / 4
    print('Err is:', err)
    print('Current leanring rate is:', currentLearningRate)
  end
  mlp:zeroGradParameters()
  mlp:backward(input, criterion:backward(mlp.output, target))
  if target == nve then
    mlp:updateParameters(currentLearningRate/1)
  else 
    mlp:updateParameters(currentLearningRate)
  end
  return err, currentLearningRate
end

-- Taken from stochastic gradient
-- Positive and negative examples taken in separately
use_random_neg = true
function mytrain(trainset, mlp)
  local iteration = 1
  local learningRate = 0.01
  local currentLearningRate = learningRate
  local learningRateDecay = 0.05
  local weightDecay = 0.0001

  print("# StochasticGradient: training")
  while true do
    local currentError = 0
    local err = 0
    local input 
    local target
    local shuffledIndices = lab.randperm(#trainset)
    for idx = 1,#trainset do
      i = shuffledIndices[idx]

      input = trainset[i].example
      target = trainset[i].label
      if target[1] == pve[1] and target[2]==pve[2] then 
        err, currentLearningRate = updatemlp(mlp, criterion, input, target, currentLearningRate)
      elseif target[1] == nve[1] and target[2] == nve[2] then
        -- We can either use the training example...
        randomVal = math.random(1,100)
        if not use_random_neg or randomVal < 25 then 
          err, currentLearningRate = updatemlp(mlp, criterion, input, target, currentLearningRate)
          --print('using real neg')
        else  -- or we can use a very simplistic negative example
          --print('using generated random neg')
          input_str = negative_example(trainset[i].s, string.len(trainset[i].s))
          input = dna_to_tensor_binary(input_str)
          target = nve
          err = updatemlp(mlp, criterion, input, target, currentLearningRate)
        end
      end

      if err == 4/0 or err == 0/0 then
        print("The error was undefined meaning the whole neural net is probably trashed")
        os.exit(2)
      end 
      currentError = currentError + err

      --randomIdx = math.random(#trainset)
      --i = randomIdx 
      --input = trainset[i].example
      --target = trainset[i].label
      --err = updatemlp(mlp, criterion, input, target, currentLearningRate)
      --currentError = currentError + err
    end
    currentError = currentError / (#trainset)
    --testErrorStr = mlperror(mlp, testset)
    testErrorStr = "Fix this"
    print("Iter #" .. iteration .. " TrainErr=" .. currentError .. 
          "  TestErr=" .. testErrorStr .. 
          " lr=" .. currentLearningRate)
    iteration = iteration + 1
    currentLearningRate = learningRate/(1+iteration*learningRateDecay)
    if maxIteration > 0 and iteration > maxIteration then
      print("# StochasticGradient: you have reached the maximum number of iterations")
      break
    end

    convolayer = mlp:get(1)
    allweights = convolayer.weight
    motifweights = {}
    for i=1,numMotifs do
      --local t = tensor.Torch(convolayer.kW, convolayer.kH, convolayer.nInputPlane)
      local t = allweights:select(4,i)
      motifweights[i] = t
    end
    --grid = getgrid(motifweights)
    --drawgrid(img,grid)
    --img:write_to_png("out.run"..runNumber..".iter"..maxIteration.."."..iteration..".png")
  end
  return mlp
end


function do_loov(fasta, trainset, mlp, trainfunc, errorfunc)
  local trainset_copy = {}
  for i=1,#trainset do
    trainset_copy[i] = trainset[i]
  end
  for i=1,#fasta do
    -- Set up the training set less one example
    local newtrainset = {}
    local testset = {}

    for j=1,#trainset_copy do
      if trainset_copy[j].bigs ~= fasta[i].s then
        newtrainset[#newtrainset+1] = trainset_copy[j]
      else 
        testset[1] = trainset_copy[j]
      end
    end

    -- Train mlp on the training set
    mlp, layers = getmlp(newtrainset, numMotifs, motifSize, motifStrs)
    --print(mlp)
    --mlp = trainfunc(newtrainset, layers)
    mlp = trainfunc(newtrainset, mlp)

    --trainErrorStr = errorfunc(mlp, newtrainset)
    trainErrorStr = "Fix this 2"

    testErrorStr = errorfunc(mlp, testset)
    print(i, trainErrorStr, testErrorStr)

    local testnet = nn.Sequential()
    for k=1,#layers do
      testnet:add(layers[k].func)
      --x = testnet:forward(trainset_copy[i].example)
      --print(layers[k].name)
      --for k2=1,x:nDimension() do
        --print("   dim "..k2..":", x:size(k2))
      --end
      --print(x)
    end

    --write_mlp(mlp, layers)
  end
end


--mlp = mytrain(trainset, mlp)
mlp = do_loov(fasta, trainset, mlp, mytrain, mlperror)

