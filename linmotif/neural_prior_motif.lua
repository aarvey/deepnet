-- Get the system modules
require("torch")
require("nn")
require("math")
require("os")
require("lcairo")
--require("gfx")

-- Get the utilities for read, parsing, etc
--dofile("../utils/cv.lua")
dofile("../utils/dirs.lua")
dofile("../utils/base.lua")
dofile("../utils/args.lua")
dofile("../utils/fasta.lua")
dofile("../utils/blosum.lua")
dofile("../utils/membership.lua")
dofile("../utils/model.lua")
dofile("../utils/draw.lua")
dofile("../utils/print.lua")


-- Get the aux functions
dofile("./mlp.lua")

runNumber = arg[1]
trainfile = arg[2]
testfile = arg[3]
maxIteration = tonumber(arg[4])
HUs = tonumber(arg[5])
numMotifs = tonumber(arg[6])
motifSize = tonumber(arg[7])

usemotiflabels = false

math.randomseed(runNumber)

function randperm(str)
  local result = {}
  local len = string.len(str)
  for i=1,len do
    r = math.random(1,len)    
    result[i] = string.sub(str,r,r)
  end
  return table.concat(result)
end


function negative_example(sidestr, len)
  while string.len(sidestr) < len + (len/5) do 
    sidestr = sidestr .. sidestr
  end
  str = randperm(sidestr)

  len = len + math.random(-len/5, len/5)
  str = string.sub(str, 1, len)
  return str
end



--------------------------------------------------
------- Take care of command line params ---------
--------------------------------------------------

fasta, motifStrs = motif_fasta_load(trainfile)
testfasta, motifStrs = motif_fasta_load(testfile)

------------------------------------------
------- Get trainset  -------
------------------------------------------

local pve
local nve
if usemotiflabels then 
  nve = torch.Tensor(numMotifs)
  for i=1,numMotifs do
    nve[i] = 0
  end
  pve = torch.Tensor(numMotifs)
  for i=1,numMotifs do
    nve[i] = 1
  end
else 
  pve = torch.Tensor(2)
  pve[1] = 1
  pve[2] = 0

  nve = torch.Tensor(2)
  nve[1] = 0
  nve[2] = 1
end

print("size of labels:", pve:size()[1], nve:size()[1])

function fasta_to_dataset(fasta, position)
  local trainset = {}
  for i=1,#fasta do
    str = fasta[i].s
    trainset[#trainset+1] = {}
    trainset[#trainset].example = dna_to_tensor_binary(str)
    trainset[#trainset].s = str
    label = torch.Tensor(numMotifs)
    numpresent = 0
    for j=1,numMotifs do
      label[j] = fasta[i].ispresent[j]
      if label[j] > 0 then numpresent = numpresent + 1 end
    end
    if usemotiflabels then
      --print("using raw label")
      trainset[#trainset].label = label
    elseif numpresent > 0 then
      --print("using pos label")
      trainset[#trainset].label = pve
    else 
      --print("using neg label")
      trainset[#trainset].label = nve
    end
  end
  function trainset:possize() return #trainset end
  return trainset
end
local trainset = fasta_to_dataset(fasta)
--local testset = fasta_to_dataset(testfasta)
print("Size of trainset is " .. #trainset)
--print("Size of testset is " .. #testset)
print("Size of first train example is:" .. trainset[1].s:len())

make_negative_examples = false
if make_negative_examples then
  for i=1,#trainset do
    print('>(CCAGGTTG,0,-1,-1); (CGATTAGC,0,-1,-1); (AAGCAATA,0,-1,-1); (TATAATGC,0,-1,-1); (ACATGCTA,0,-1,-1); ')
    x = negative_example(trainset[i].s,string.len(trainset[i].s))
    print(x)
  end
  os.exit(0)
end

------------------------------------------
-------------- Run on trainset -----------
------------------------------------------

mlp, layers = getmlp(trainset,HUs,motifSize)

 
winsize = 400
--w = lcairo.Window(winsize,winsize)
--cr = lcairo.Cairo(w)
img = lcairo.Image(winsize,winsize)
cr = lcairo.Cairo(img)


function iscloser(pred, lab1, lab2) 
  dist1 = (pred[1] - lab1[1])^2 + (pred[2] - lab1[2])^2
  dist2 = (pred[1] - lab2[1])^2 + (pred[2] - lab2[2])^2
  if dist1 < dist2 then
    return true
  end
  return false

  --d = pred:nDimension()
  --for i=1,d do 
  --end
end

function mlperror(mlp, testset)
  local criterr = 0
  local criterion = nn.AbsCriterion()
  local perr = 0
  local nerr = 0
  local nump = 0
  local numn = 0
  for i=1,#testset do
    --print('Testset value', i, testset[i].s, testset[i].label)
    input = testset[i].example
    target = testset[i].label
    pred = mlp:forward(input)
    err = criterion:forward(pred, target)
    criterr = criterr + err
    --print(target[1], target[2], target:nDimension(), target:size()[1])
    --print(pve[1], pve[2])
    --print(nve[1], nve[2])
    if target[1] == pve[1] and target[2] == pve[2] then
      --print(pred, pve, nve)
      if not iscloser(pred, pve, nve) then 
        perr = perr + 1
      end
      nump = nump + 1
    elseif target[1] == nve[1] and target[2] == nve[2] then
      --print(pred, nve, pve)
      if not iscloser(pred, nve, pve) then 
        nerr = nerr + 1
      end
      numn = numn + 1
    else
      print("Can't recognize target!")
    end
  end
  --print('errs: ', perr, nerr)
  --print('nums: ', nump, numn)
  if nump + numn ~= #testset then
    print("Number of examples don't add up!")
  end
  totalerr = (perr+nerr) / (nump + numn)
  criterr = criterr/#testset
  perr = nump>0 and perr/nump or 0
  nerr = numn>0 and nerr/numn or 0
  s = " " .. perr  .. " " .. nerr .. " " .. totalerr
  return s
end


function updatemlp(mlp, criterion, input, target, currentLearningRate)
  criterion = nn.MSECriterion()
  err = criterion:forward(mlp:forward(input), target)
  mlp:zeroGradParameters()
  mlp:backward(input, criterion:backward(mlp.output, target))
  if target == nve then
    mlp:updateParameters(currentLearningRate/1)
  else 
    mlp:updateParameters(currentLearningRate)
  end
  return err
end

-- Taken from stochastic gradient
-- Positive and negative examples taken in separately
function mytrain(trainset, mlp)
  local iteration = 1
  local learningRate = 0.01
  local currentLearningRate = learningRate
  local learningRateDecay = 0.01
  local weightDecay = 0.0001

  print("# StochasticGradient: training")
  while true do
    local currentError = 0
    local err = 0
    local input 
    local target
    local shuffledIndices = lab.randperm(#trainset)
    for idx = 1,#trainset do
      i = shuffledIndices[idx]

      input = trainset[i].example
      target = trainset[i].label
      err = updatemlp(mlp, criterion, input, target, currentLearningRate)
      currentError = currentError + err

      --input_str = negative_example(trainset[i].s, string.len(trainset[i].s))
      --input = dna_to_tensor_binary(input_str)
      --target = nve
      --err = updatemlp(mlp, criterion, input, target, currentLearningRate)
      --currentError = currentError + err

      --randomIdx = math.random(#trainset)
      --i = randomIdx 
      --input = trainset[i].example
      --target = trainset[i].label
      --err = updatemlp(mlp, criterion, input, target, currentLearningRate)
      --currentError = currentError + err
    end
    currentError = currentError / (#trainset)
    --testErrorStr = mlperror(mlp, testset)
    testErrorStr = "Fix this"
    print("Iter #" .. iteration .. " TrainErr=" .. currentError .. 
          "  TestErr=" .. testErrorStr .. 
          " lr=" .. currentLearningRate)
    iteration = iteration + 1
    currentLearningRate = learningRate/(1+iteration*learningRateDecay)
    if maxIteration > 0 and iteration > maxIteration then
      print("# StochasticGradient: you have reached the maximum number of iterations")
      break
    end

    convolayer = mlp:get(1)
    allweights = convolayer.weight
    motifweights = {}
    for i=1,numMotifs do
      --local t = tensor.Torch(convolayer.kW, convolayer.kH, convolayer.nInputPlane)
      local t = allweights:select(4,i)
      motifweights[i] = t
    end
    --grid = getgrid(motifweights)
    --drawgrid(img,grid)
    --img:write_to_png("out.run"..runNumber..".iter"..maxIteration.."."..iteration..".png")
  end
  return mlp
end


function do_loov(trainset, mlp, trainfunc, errorfunc)
  local trainset_copy = {}
  for i=1,#trainset do
    trainset_copy[i] = trainset[i]
  end
  for i=1,#trainset do
    -- Set up the training set less one example
    local newtrainset = {}
    for j=1,#trainset_copy do
      if j==i then
        -- do nothing
      elseif j>i then
        newtrainset[j-1] = trainset_copy[j]
      elseif j<i then
        newtrainset[j] = trainset_copy[j]
      end
    end

    -- Train mlp on the training set
    mlp, layers = getmlp(newtrainset, numMotifs, motifSize)
    --print(mlp)
    --mlp = trainfunc(newtrainset, layers)
    mlp = trainfunc(newtrainset, mlp)
    --trainErrorStr = errorfunc(mlp, newtrainset)
    trainErrorStr = "Fix this 2"
    local testset = {}
    testset[1] = trainset_copy[i]
    --print('Testset is:', testset[1].s, testest[1].label)
    testErrorStr = errorfunc(mlp, testset)
    print(i, trainErrorStr, testErrorStr)
    --write_mlp(mlp, layers)
  end
end


--mlp = mytrain(trainset, mlp)
mlp = do_loov(trainset, mlp, mytrain, mlperror)

