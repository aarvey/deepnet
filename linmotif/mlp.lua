dofile("../utils/editconvo.lua")

-- Get the functions for neural net construction/execution
function getmlp(trainset, HUs, motif_size, motifStrs)
  local xtrn_example = trainset[1].example
  local layers = {}

  -- First layer is convolution
  --print("Size of training example tensor:")
  --print("   dim 1:", xtrn_example:size(1))
  --print("   dim 2:", xtrn_example:size(2))
  --print("   dim 3:", xtrn_example:size(3))

  local testnet = nn.Sequential()

  ---------------------------------
  layers[#layers+1] = {}
  layers[#layers].func = nn.SpatialConvolution(1, HUs, motif_size, xtrn_example:size(2))
  layers[#layers].name = "SC" .. HUs .. "_KW" .. motif_size
  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  --print("Convolution HU tensor size")
  --print("   dim 1:", x:size(1))
  --print("   dim 2:", x:size(2))
  --print("   dim 3:", x:size(3))


  ---------------------------------
  layers[#layers+1] = {}
  layers[#layers].func = nn.Max(1)
  layers[#layers].name = "Max"

  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  --print("Max of the HU has tensor size")
  --print("   dim 1:", x:size(1))
  --print("   dim 2:", x:size(2))

  ---------------------------------
  layers[#layers+1] = {}
  layers[#layers].func = nn.Reshape(HUs)
  layers[#layers].name = "Reshape"

  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  --print("Reshape of max of the HU has tensor size")
  --print("   dim 1:", x:size(1))

  ---------------------------------
  --layers[#layers+1] = {}
  --layers[#layers].func = nn.Mean(1)
  --layers[#layers].name = "Mean over motifs"

  ---------------------------------
  layers[#layers+1] = {}
  layers[#layers].func = nn.Linear(HUs,HUs)
  layers[#layers].func.weightDecay=0.001
  layers[#layers].name = "Linear to classes"

  ---------------------------------
  layers[#layers+1] = {}
  layers[#layers].func = nn.Linear(HUs,2)
  layers[#layers].func.weightDecay=0.001
  layers[#layers].name = "Linear to classes"

  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  --print("Final classes is")
  --print("   dim 1:", x:size(1))
  --print('Classes are:', x)

  mlp=nn.Sequential()
  for i=1,#layers do
    mlp:add(layers[i].func)
    --print("Added layer " .. layers[i].name)
  end


  convolayer = mlp:get(1)
  allweights = convolayer.weight
  --print('All weights non-zero', allweights)

  --print('Before Breakup', #motifStrs, table.tostring(motifStrs))
  motifStrs = breakup_strings(motifStrs, motif_size,10)
  --print('After Breakup', #motifStrs, table.tostring(motifStrs))
  insert_motifs(mlp, motifStrs, false)

  convolayer = mlp:get(1)
  allweights = convolayer.weight
  --print('All weights zero', allweights)

  return mlp, layers
end
