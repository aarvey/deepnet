require "torch"
require "nn"
dofile("utils/base.lua")
dofile("utils/args.lua")
dofile("utils/fasta.lua")
dofile("utils/blosum.lua")
dofile("utils/membership.lua")
dofile("datadir.lua")

fasta_fname =datadir.."scop.1.59.95.fasta"
labels_fname=datadir.."allseq_membership.table"
blosum_fname=datadir.."BLOSUM62"

compargs= {
  {mode	  	="train",__valid={"train","test"}},
}
optargs={
  {method	= "tpmax",__valid={"tpmean","tpmax"}},
  {HUs		= 10	},
  {lr		= 0.1	},
  {maxIteration = 500   },
  {startEpoch	= 0     },
  {features	= "binary",__valid={"binary","blosum"}},
  {ext		= ""},	--model fname extension
  {testModel 	= "models/a.model"},
}
pargs=read_args(compargs,optargs)
for k,p in pairs(pargs) do print(k,p) end

blosumX,blosumAminos=read_blosum(blosum_fname)
fasta=fasta_load(fasta_fname)
labels,examples=membership_load(labels_fname)


function make_name(epoch,ext) 
   local fname=datadir.."models/"
   fname = fname .. pargs.method .. "_".. pargs.features
   if method ~= "linear" then
	fname = fname .. "_HUs" .. pargs.HUs
   end
   fname=fname .."_epoch".. epoch .. "_lr" .. pargs.lr.."_"..pargs.ext;
   fname = fname .. ext
   return fname
end


criterion = nn.MSECriterion()
trainer = {};
trainer.learningRate = pargs.lr
trainer.maxIteration = pargs.maxIteration -- epochs
currentExample=0;


function save_model(fname)
 bin = torch.Archiver(fname, "w")
 bin:binary()
 bin:writeObject(mlp) 
end

function load_model(fname)
   bin = torch.Archiver(fname, "r")
   bin:binary()
   mlp = bin:readObject()
end

function protein_to_tensor(p)
	if pargs.features=="binary" then
		return protein_to_tensor_binary(p)
	elseif pargs.features=="blosum" then
		return protein_to_tensor_blosum(p, blosumX, blosumAminos)
	else
		print("unrecognised feature type")
		os.exit(1)
	end
end

function error_rate( xe,ye )

  local ncorrect=0
  for i=1,#xe do
	x= protein_to_tensor(xe[i])
	pred=mlp:forward(x);
	print(	string.format("%.04f",pred[1]),
		string.format("%.04f",pred[2]),
		string.format("%.04f",ye[i][1]),
		string.format("%.04f",ye[i][2]))
	if     pred[1] > pred[2] and ye[i][1]>0 then 
		ncorrect=ncorrect+1 
	elseif pred[2] > pred[1] and ye[i][2]>0 then
		ncorrect=ncorrect+1 
	end
  end
  return (#xe-ncorrect)/#xe
end

-------------------------



if pargs.mode=="test" then

--[[
   model=load_model(datadir..pargs.testModel)
   errate = error_rate(xtsts, ytsts)
   print("error_rate:",errate)
   print("num_examples:",#xtsts)
]]--

elseif pargs.mode=="train" then
   --train
   local  pve=torch.Tensor(2) pve[1]=1 pve[2]=0
   local  nve=torch.Tensor(2) nve[1]=0 nve[2]=1

   local class=1 -- do all eventually
   xtsts={} ytsts={}
   xtrns={} ytrns={}
   for i=1,#examples do
   	if examples[i].name ~= fasta[i].name then
   		error("name mismatch, fasta to membership table: ",examples[i].name, fasta[i].name)
	end
	local m = examples[i].member[class]
	if m==0 then --ignore
	elseif m==1 then
		xtrns[#xtrns+1]=fasta[i].s
		ytrns[#ytrns+1]=pve
	elseif m==2 then
		xtrns[#xtrns+1]=fasta[i].s
		ytrns[#ytrns+1]=nve
	elseif m==3 then
		xtsts[#xtsts+1]=fasta[i].s
		ytsts[#ytsts+1]=pve
	elseif m==4 then
		xtsts[#xtsts+1]=fasta[i].s
		ytsts[#ytsts+1]=nve
	else 
		error("what??") 
	end
   end


   local xtrn_example=protein_to_tensor(xtrns[1])   -- use for feature dimensions 

   epoch = pargs.startEpoch
   if epoch==0 then
	num_classes=2
	window_sz=5  
	mlp=nn.Sequential()
 	mlp:add( nn.SpatialConvolution(1, pargs.HUs, window_sz, xtrn_example:size(2)) )

	if 	pargs.method=="tpmean" then 
 		mlp:add( nn.Mean(1) )  
	elseif 	pargs.method=="tpmax" then
 		mlp:add( nn.Max(1) )  
	else
		print("method err") 
		os.exit(1) 
	end
 	mlp:add( nn.Reshape(pargs.HUs) ) 
 	mlp:add( nn.Linear(pargs.HUs,num_classes)   )  

   else
	--start from arg[4]
	load_model(make_name(epoch,".model"))
   end
   
   -- quick test 
   --trn_examples=10001

   for epoch = epoch+1,trainer.maxIteration do
     model_name = make_name(epoch,".model")
     error_name = make_name(epoch,".error")
     msError=0; 
     for j = 1,#xtrns do
	
	ex=math.floor(math.random()*#xtrns)+1
	y= ytrns[ex]
	x= protein_to_tensor(xtrns[ex])
	pred=mlp:forward(x);

	example = {}; example[1]=x; example[2]=y;
        local err=  criterion:forward(pred, example); 
	msError = msError + err;
	currentExample = currentExample + 1;
	mlp:zeroGradParameters();
	mlp:backward(criterion:backward(mlp.output, example), x); 

	mlp:updateParameters(trainer.learningRate);
--	for i=1,mlp:size() do
--		sz = mlp:get(i).output:size(1)
--		mlp:get(i):updateParameters(trainer.learningRate/sz)
--	end
	
	if (currentExample % 10000) ==0 then -- output MSE every 10000 examples
	  collectgarbage()
          print(model_name.." example:"..j.."/"..#xtrns.."\tcurMSE:",msError/j) 
        end
     end -- for j

     save_model(model_name); 
      
     mseError = msError/#xtrns
     testError = error_rate(xtsts,ytsts)
     trainError = error_rate(xtrns,ytrns)
     f=io.open(error_name,"w+")
     f:write(mseError.."\t"..testError.."\t"..trainError.."\n")
     print("mse_err",mseError,"train_err",trainError,"test_err",testError)
     f:close()

     print("[saved model]:"..model_name)

   end -- for epoch
else  --mode = train
   error()
end

