
function read_blosum(filename)
	local f = io.open(filename)

	-- comments
	l=f:read()
	while string.sub(l,1,1)=="#" do
		l=f:read()
	end

	-- amino list
	local aminos={}
	local num_aminos=0
	for s in string.gmatch(l,"%S") do
		num_aminos=num_aminos+1
		aminos[s]=num_aminos
	end
	
	-- matrix
	local x = torch.Tensor(num_aminos,num_aminos,1)
	for j=1,num_aminos do
		l = f:read()
		local i=0
		for s in string.gmatch(l,"[0-9-]+") do
			i=i+1
			x[j][i][1]=tonumber(s)
		end
		if i~=num_aminos then print("error parsing blosum") os.exit(1) end
	end

	--normalise - not tried yet
	x=x*(1/x:norm())

	f:close()

	return x,aminos  -- tensor and table lookup amino char to column/row in tensor index
end
