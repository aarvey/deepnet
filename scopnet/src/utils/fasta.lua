
aminos=23
Abyte = string.byte("A")

function fasta_load( fasta_fname )
  local fin = io.open(fasta_fname,"r")
  local allpr={}
  local l=fin:read()  -- first protein
  while 1 do
	local nam,fam,com= string.match(l,">(%S+)%s+(%S+)%s(.*)")
	local pr={name=nam, family=fam, comment=com,  s=""}
	while 1 do
		l=fin:read() 
		if not l then break end
		if string.sub(l,1,1) == '>' then break end -- new protein
		pr.s=pr.s..l
	end
	allpr[#allpr+1]=pr
	if not l then break end
  end
  fin:close()
  return allpr
end


function protein_to_tensor_binary( p )
	-- turn protein string into nx23 Tensor
	local aminos=23
	p = string.upper(p)
	p = string.gsub(p,"V","O")
	p = string.gsub(p,"W","J")
	p = string.gsub(p,"Y","B")
	p = string.gsub(p,"Z","V") 
	p = string.gsub(p,"X","W") 
	lp = string.len(p)
	x=torch.Tensor(lp,aminos,1)
	x:zero()
	for i=1,lp do
		c=string.sub(p,i,i)
		vi=string.byte(c)-Abyte+1
		x[i][vi][1]=1
	end
	return x
end

function protein_to_tensor_blosum( p, blosum, blosumAminos )
	lp = string.len(p)
	x=torch.Tensor(lp,blosum[1]:size(1),1)
	x:zero()
	for i=1,lp do
		c=string.sub(p,i,i)
		bi = blosumAminos[c] 
		x[i]:add(blosum[bi])

--		for j=1,x:size(2) do
--	  	  x[i][j][1] = blosum[bi][j][1]
--		end
	end
	return x
end
