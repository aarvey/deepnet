-- Get the functions for neural net construction/execution
function getmlp(trainset, HUs, kmersize, embedsize, mlptype)
  local xtrn_example = trainset[1].example
  local layers = {}
  local numclasses = 2

  --First layer is histogram of kmersizes
  print("Size of training example tensor:")
  for i=1,xtrn_example:nDimension() do
    print("   dim "..i..":", xtrn_example:size(i))
  end

  local testnet = nn.Sequential()

  --------------------------------
  layers[#layers+1] = {}
  if mlptype == 1 then 
    layers[#layers].func = nn.Linear(4^kmersize, numclasses)
    layers[#layers].name = "nn.Linear(4^"..kmersize..", "..numclasses..")"
  elseif mlptype >= 2 then
    layers[#layers].func = nn.Linear(4^kmersize, embedsize)
    layers[#layers].name = "nn.Linear(4^"..kmersize..", "..embedsize..")"
  end

  testnet:add(layers[#layers].func)
  x = testnet:forward(xtrn_example)
  print("After first layer (linear embedding) tensor size")
  for i=1,x:nDimension() do
    print("   dim "..i..":", x:size(i))
  end

  ---------------------------------

  layers[#layers+1] = {}
  if mlptype == 2 then 
    layers[#layers].func = nn.Linear(embedsize,numclasses)
    layers[#layers].name = "Linear embedsize ("..embedsize..") to hidden units ("..numclasses..")"
  elseif mlptype >= 3 then 
    layers[#layers].func = nn.Linear(embedsize,HUs)
    layers[#layers].name = "Linear embedsize ("..embedsize..") to hidden units ("..HUs..")"
    layers[#layers+1] = {}    
    layers[#layers].func = nn.Tanh()
    layers[#layers].name = "Tanh"
  end

  ---------------------------------

  layers[#layers+1] = {}
  if mlptype == 3 then
    layers[#layers].func = nn.Linear(HUs,numclasses)
    layers[#layers].name = "Linear HUs ("..HUs..") to HUs ("..numclasses..")"
  elseif mlptype >= 4 then
    layers[#layers].func = nn.Linear(HUs,HUs)
    layers[#layers].name = "Linear HUs ("..HUs..") to HUs ("..HUs..")"
    layers[#layers+1] = {}
    layers[#layers].func = nn.Tanh()
    layers[#layers].name = "Tanh"
  end

  layers[#layers+1] = {}
  if mlptype==4 then 
    layers[#layers].func = nn.Linear(HUs,numclasses)
    layers[#layers].name = "Linear HUs ("..HUs..") to numclasses ("..numclasses..")"
  elseif mlptype >= 5 then
    print("MLP style is too high!")
    os.exit(2)
  end

  mlp=nn.Sequential()
  for i=1,#layers do repeat
    islayer = false
    for k,v in pairs(layers[i]) do islayer = true end
    if not islayer then  break end  -- continue
    mlp:add(layers[i].func)
    print("Added layer " .. layers[i].name)
  until true end

  return mlp, layers
end
