require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("/home/aarvey/.path.lua")

dofile(path.utils .. "/fasta.lua")
dofile(path.utils .. "/readcsv.lua")
dofile(path.utils .. "/embed.lua")
dofile(path.utils .. "/kmer.lua")

-----------------------------------
if (#arg < 2) then
  print("combine_dim trainfile1 trainfile2 trainfile3 ... outfile");
  print("")
  print("      Combine the output dimensions of all the files")
  print("      Write the combined mlp to outfile")
  return
end
-----------------------------------

-----------------------------------

--
-- Read in the mlps
--
mlps = {}
for i=1,#arg-1 do 
  fname = arg[i]
  print("Reading fname from", fname)
  full_mlp = mlp_from_file(fname)
  mlps[i] = full_mlp:get(1):get(1)
end
outfname = arg[#arg]

--
-- Combine the mlps into one space
--
combined_mlp = nn.Sequential()
indim=4096
outdim=5
combined_mlp:add(nn.Linear(indim,outdim*#mlps))
combined_weight = combined_mlp:get(1).weight
combined_bias = combined_mlp:get(1).bias

combined_weight:zero()
combined_bias:zero()

combined_weight = combined_mlp:get(1).weight
combined_bias = combined_mlp:get(1).bias

k = 1
for i=1,#mlps do
  mlp_outdim = mlps[i]:get(1).bias:size()[1]
  for j=1,mlp_outdim do
    --print('i',i,'k',k,'j',j)
    single_column = mlps[i]:get(1).weight:select(2,j)
    for ii=1,single_column:size()[1] do
      combined_weight[ii][k] = single_column[ii]
    end
    --print('Original', single_column)
    --print('Combined weight is:',combined_weight)

    -- set bias vector
    combined_bias[k] = mlps[i]:get(1).bias[j]
    k = k + 1
  end
  --print("original bias", mlps[i]:get(1).bias)
end


x = torch.Tensor(indim)
x:fill(1)
for i=1,#mlps do
  y = mlps[i]:forward(x)
  --print("output original mlp " .. i, y)
end
y = combined_mlp:forward(x)
--print("output combined mlp", y)

outdim = 30
combined_mlp:add(nn.Linear(y:size(1),outdim))

prl = nn.ParallelTable()
prl:add(combined_mlp)
prl:add(combined_mlp)

mlp=nn.Sequential()
mlp:add(prl)
mlp:add(nn.PairwiseDistance(1))

f = torch.DiskFile(outfname, "w")
f:writeObject(mlp) 
f:close()

outdim = y:size(1)
print("output dim of combination is: " .. outdim)

