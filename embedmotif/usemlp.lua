require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("../utils/fasta.lua")
dofile("../utils/readcsv.lua")
dofile("../utils/embed.lua")
dofile("../utils/kmer.lua")

----------------------------------
if #arg < 3 then 
  print("Usage: lua usemlp.lua MLPFILE  FASTAFILE  EMBEDFILE")
  os.exit()
end

-----------------------------------
mlp_fname = arg[1]
print("Reading embedding mlp from " .. mlp_fname)

f = torch.DiskFile(mlp_fname, "r")
mlp = f:readObject() 
f:close()

---------------------------------------------------

chop_len = 0
if #arg > 4 then
  chop_len = tonumber(arg[4])
  slide_len = tonumber(arg[5])
  print("chop length is", chop_len)
end

--------------------------------------------  

datafile = arg[2]
fasta, motifStrs = motif_fasta_load(datafile)
print("Finished reading fasta file")

print("Finished converting fasta file to dataset")
print("Number of sequences " .. #fasta)
print("Size of first sequence is:" .. fasta[1].s:len())

newfasta = {}
j = 1
if chop_len > 0 then
  for i=1,#fasta do
    repeat
      newfasta[j] = {}
      start = (j-1)*slide_len+1
      fin = start + chop_len - 1
      newfasta[j].s = fasta[i].s:sub(start, fin)
      newfasta[j].comment = fasta[i].comment .. "chopped from " ..  start .. "-" .. fin
      j = j + 1
    until start + chop_len >= fasta[i].s:len()
  end
end
fasta = newfasta

print("Number of chopped sequences " .. #fasta)
print("Size of first chopped sequence is:" .. fasta[1].s:len())

kmersize = 6
rawdata = {}
for i=1,#fasta do
  fasta[i].example = example_to_kmers(fasta[i].s, kmersize)
  rawdata[i] = fasta[i].example
  indim = 4^kmersize
  s = fasta[i].comment
  name = s:sub(8,s:len())
  fasta[i].name = name
  print(fasta[i].name)
  --rawdata[i] = indices[i]
  --indim = #rawdata
end


--------------------------------------------  

-- Compatibility: Lua-5.1
function split(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

embedfile = arg[3]
exist_proj = {}
for line in io.lines(embedfile) do
  s = split(line, ": ")
  crm = s[1]
  coords = split(s[2], " ")
  --print(crm, coords[1], coords[2])
  x = tonumber(coords[1])
  y = tonumber(coords[2])
  exist_proj[#exist_proj+1] = {crm=crm, proj={x,y}}
end
print("Finished reading embedding")


----------------------------------------------------


function cmp_dist(x, y)
  return x.d < y.d
end

function knn(exist_proj, tofind) 
  dist = {}
  for i=1,#exist_proj do
    d = lpdist(tofind.proj, exist_proj[i].proj, 2)
    dist[i] = {d=d, crm=exist_proj[i].crm}
  end
  table.sort(dist, cmp_dist)
  return dist
end


for i=1,#fasta do repeat
  --print("name", fasta[i].name)
  --print(fasta[i].name:sub(1,3))
  --if fasta[i].name:sub(1,3)~="eve" then break end -- continue
  projection = mlp:get(1):get(1):forward(fasta[i].example)
  --outline = ""
  --for ek=1,embed:size(1) do
  --  outline = string.format("%s%.7f ",  outline, embed[ek])
  --  end
  --  outline = outline:sub(1,outline:len()-1) .. "\n"
  --  --print("outline:", outline)
  --end
  --print("projection:", projection)
  --print('')
  x = projection[1]
  y = projection[2]
  print('>',fasta[i].name,x,y)
  dists = knn(exist_proj, {crm=fasta[i].comment, proj={x,y}})
  for j=1,#dists do
    --print(dists[j].d, dists[j].crm)
    if j > 10 then break end
    --if dists[j].d > 0.15 then break end
  end
until true end


