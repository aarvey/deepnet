require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("/home/aarvey/.path.lua")

dofile(path.utils .. "/fasta.lua")
dofile(path.utils .. "/readcsv.lua")
dofile(path.utils .. "/embed.lua")
dofile(path.utils .. "/kmer.lua")
dofile(path.utils .. "/graph.lua")

-----------------------------------
if (#arg < 2) then
  print("embed_hist <multi-adj-matrix> <raw-filename>");
  print("")
  print("      Where <multi-adj-filename> is an adjacency matrix that can contain")
  print("      any natural number (multiple edges) with the following format:")
  print("      0, 1, 0, 2, ..., 0;")
  print("      Where there are the same number of rows and columns")
  print("")
  print("      If specified the <raw-filename> is data in vector format")
  print("      x1, x2, x3, ... xn;")
  print("      Where x may be an image, feature space, or any other data")
  return
end
-----------------------------------

-----------------------------------
namesfname = arg[4]
print("Name file: " .. namesfname)
names = {}
local file = io.open(namesfname,"r")   
i = 1
while true do
    local line = file:read("*l")
    if line == nil then break end
    names[i] = line
    print(i,names[i], line)
    i = i + 1
end
-----------------------------------
adjmatrixfname = arg[1]
print("Reading graph file: " .. adjmatrixfname)
adjmatrix, matdim = readcsv(adjmatrixfname)
print("Finished reading " .. adjmatrixfname)
simtable, matdim = adjmatrix2pairs(adjmatrix)

examplefname = arg[2]
stemfname = examplefname
print("Reading feature file: " .. examplefname)
rawdata, indim = readcsv(examplefname)
print(#rawdata, featuresdim)
print("Feature[1]", rawdata[1])
print("Finished reading feature file: " .. examplefname)

print("Matrix dimension: " .. matdim)
maxpoint = -9
local s = ""
for i=1,#simtable do
  s = ""
  for j=1,matdim do 
    simtable[i][j] = simtable[i][j] + 1 
    if simtable[i][j] > maxpoint then
      maxpoint = simtable[i][j]
    end
    --s = s .. " " .. simtable[i][j]
  end
  --print(i,s)
end

maxpoint = #adjmatrix
numpoints = maxpoint
print("Number of points: " .. numpoints)
maxpoint = 50
maxpoint = numpoints
-----------------------------------
outdim = tonumber(arg[3])
print("Output dimension: " .. outdim)
-----------------------------------

starting_mlp_fname = nil
if #arg >= 5 then 
  starting_mlp_fname = arg[5]
  print ("Starting with old mlp: " .. starting_mlp_fname)
end

----------------------------------------------
print("In dimension: " .. indim)
-----------------------------------
layer_hus = {}
for i=6,#arg do
  layerdim = tonumber(arg[i])
  layer_hus[i-5] = {}
  layer_hus[i-5].dim = layerdim
  print("Layer dimension: " .. layerdim)
end
  print("Layer hus has elts: ", #layer_hus)
  for i=1,#layer_hus do
    print("layer hu is:", layer_hus[i].dim)
  end
-------------------------------------
do_random_mlp = false
print("Randomize MLP: ", do_random_mlp)
-----------------------------------


-- Criterion for measuring similarity.
-- Value of +1 means similar
-- Value of -1 means dissimilar
criterion = nn.HingeEmbeddingCriterion(outdim/3)

print("Starting to set up examples")


posexamples = {}
negexamples = {}
if not is_two_labels then
  for i=1,maxpoint do
    for j=1,maxpoint do repeat 
      if i > maxpoint or j > maxpoint then break end -- continue
      if adjmatrix[i][j] == 0 then 
        negexamples[#negexamples+1] = {}
        negexamples[#negexamples][1] = {rawdata[i], rawdata[j]}
        negexamples[#negexamples][2] = -1
      elseif adjmatrix[i][j] > 0 then
        posexamples[#posexamples+1] = {}
        posexamples[#posexamples][1] = {rawdata[i], rawdata[j]}
        posexamples[#posexamples][2] = 1
      end
    until true end
  end
else 
  for i=1,maxpoint do repeat
    is_positive = {}
    for j=1,maxpoint do
      if adjmatrix[i][j] == 1 then
        is_positive[#is_positive+1] = j
      end
    end
    if #is_positive == 0 then
      break
    end
    for j=1,maxpoint do
      if adjmatrix[i][j] == 1 then
        posexamples[#posexamples+1] = {}
        posexamples[#posexamples][1] = {rawdata[i], rawdata[j]}
        posexamples[#posexamples][2] = 1
      elseif adjmatrix[i][j] == 0 then
        negexamples[#negexamples+1] = {}
        negexamples[#negexamples][1] = {rawdata[i], rawdata[j]}
        negexamples[#negexamples][2] = -1
      end
    end
  until true end
end

print("Examples are now set up")
print("Number of positive examples: " .. #posexamples)
print("Number of negative examples: " .. #negexamples)

posdup=1
negdup=1

mlp = getembedmlp(indim, outdim, starting_mlp_fname, layer_hus)

if do_random_mlp then 
  mlp = getembedmlp(indim, outdim)
  modelfname = "random1.mlp"
  f = torch.DiskFile(modelfname, "w")
  f:writeObject(mlp) 
  f:close()

  mlp = getembedmlp(indim, outdim)
  modelfname = "random2.mlp"
  f = torch.DiskFile(modelfname, "w")
  f:writeObject(mlp) 
  f:close()

  mlp = getembedmlp(indim, outdim)
  modelfname = "random3.mlp"
  f = torch.DiskFile(modelfname, "w")
  f:writeObject(mlp) 
  f:close()
  
  os.exit()
end

numpoints = maxpoint
trainembedding(mlp, criterion, indices, rawdata, posexamples, negexamples, stemfname)









