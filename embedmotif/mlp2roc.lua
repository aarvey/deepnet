require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("../utils/fasta.lua")
dofile("../utils/readcsv.lua")
dofile("../utils/embed.lua")
dofile("../utils/kmer.lua")

function tensor2table(t) 
  tab = {}
  for j=1,t:size(1) do
    tab[j] = t[j]
  end
  return tab
end

-- Compatibility: Lua-5.1
function split(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end



----------------------------------
if #arg < 8 then 
  print("Usage: lua mlp2ranks.lua MLPFILE  ALLSET  TESTSET ADJMATRIX [RANDOMIZE] [EUCLIDEAN] [TRANSFAC] [USEFPR]")
  os.exit()
end

--------------------------------------------

detection_cutoffs = {}
for i=1,40 do
  detection_cutoffs[i] = i / 200
end
for i=21,100 do
  detection_cutoffs[i+20] = i / 100
end
print("num edges cutoffs",table.tostring(detection_cutoffs))
s = ""
for i=1,#detection_cutoffs do
  detection = detection_cutoffs[i]
  s = s .. detection .. ","
end
print("DETECTION levels", s)
--------------------------------------------

randomize = 0
if #arg >= 5 then 
  randomize = tonumber(arg[5])
end
print("randomize:",randomize)

--------------------------------------------  
use_euclidean = 0
if #arg >=6 then 
  use_euclidean = tonumber(arg[6])
end
print("use euclidean:", use_euclidean)

-------------------------------------------- 
use_transfac = false
if #arg >=7 then 
  use_transfac = tonumber(arg[7])
end
print("Use transfac:", use_transfac)

use_fpr = false
if #arg >=8 then 
  use_fpr = tonumber(arg[8])
end
print("Make a ROC where x axis is FPR (not detection):", use_fpr)

-------------------------------------------- 

mlp_fname = arg[1]
ext = mlp_fname:sub(mlp_fname:len()-2, mlp_fname:len())
if ext == ".gz" then
  cmd = "gzip -d " .. mlp_fname
  os.execute(cmd)
  mlp_fname = mlp_fname:sub(1,mlp_fname:len()-3)
end
print("Reading embedding mlp from " .. mlp_fname)
f = torch.DiskFile(mlp_fname, "r")
	mlp = f:readObject() 
f:close()

--------------------------------------------  

allsetfile = arg[2]
testsetfile = arg[3]
allset = {}
if use_transfac > 0 then
  print("Reading TF file")
  allset, indim = readtfs(allsetfile)
  testset, indim = readtfs(testsetfile)
else 
  print("Reading fasta file: " .. allsetfile)
  allset, motifStrs = motif_fasta_load(allsetfile)
  print("Reading fasta file: " .. testsetfile)
  testset, motifStrs = motif_fasta_load(testsetfile)
end 
print("Finished reading fasta files")
print("Number of allset sequences " .. #allset)
print("Number of test sequences " .. #testset)

--print(table.tostring(allset))

kmersize = 6
for i=1,#allset do
  if use_transfac > 0 then 
    allset[i].example = allset[i].example
  else 
    allset[i].example = example_to_kmers(allset[i].s, kmersize)
  end
  s = allset[i].comment
  name = s:sub(2,s:len())
  allset[i].name = name
  --print("'" .. name .. "'")
  --print(allset[i].name)
  --break
end
print("Finished converting fasta file to dataset")

kmersize = 6
for i=1,#testset do
  if use_transfac > 0 then 
    testset[i].example = testset[i].example
  else 
    testset[i].example = example_to_kmers(testset[i].s, kmersize)
  end
  s = testset[i].comment
  name = s:sub(2,s:len())
  testset[i].name = name
  --print(testset[i].name)
  --break
end
print("Finished converting fasta file to dataset")

exist_proj = {}
for i=1,#allset do
  --print("Size of example: " .. allset[i].example:size(1))
  --print(allset[i].example)
  projection = mlp:get(1):get(1):forward(allset[i].example)
  exist_proj[#exist_proj+1] = {crm=allset[i].comment, proj=tensor2table(projection), ex=tensor2table(allset[i].example)}
  --break
end

--------------------------------------------

adjmatrixfname = arg[4]
print("Reading " .. adjmatrixfname)
adjmatrix, indim = readcsv(adjmatrixfname)
print("Finished reading " .. adjmatrixfname)

--------------------------------------------


function getranks(dists, thisproj, thisidx, adjmatrix, randomize)
  sens = {}
  fprs = {}
  precs = {}
  for i=1,#detection_cutoffs do
    sens[i] = 0
    fprs[i] = 0
    precs[i] = 0
  end
  sens[#detection_cutoffs] = 1
  fprs[#detection_cutoffs] = 1
  precs[#detection_cutoffs] = 0
  detection_counter = 1

  other_links = adjmatrix[thisidx]
  other_links = tensor2table(other_links)
  other_links[thisidx] = 0
  if randomize~=0 then 
    print("randomizing edges")
    randidx = lab.randperm(#other_links)
    tmp = {}
    for i=1,randidx:size(1) do
      tmp[i] = other_links[randidx[i]]
    end 
    other_links = tmp
  end
  other_links[thisidx] = 0
  if #dists ~= #other_links then
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    print("we have too many links or distances!")
    return
  end
  total_true_pos = 0
  for i=1,#other_links do
    if other_links[i] > 0 then
      total_true_pos = total_true_pos + 1
    end
  end
  total_true_neg = #other_links - total_true_pos
  fpr = 0.0
  ranks = {}
  for i=1,#dists do
    if (other_links[dists[i].idx] > 0) then 
      ranks[#ranks+1] = {rank=i, dist=dists[i], fpr=fpr}
      --print(fpr, dists[i].d, dists[i].crm, dists[i].idx)
    else 
       fpr = fpr + 1
    end
    if i/#dists > detection_cutoffs[detection_counter] then 
      sens[detection_counter] = #ranks / total_true_pos
      fprs[detection_counter] = fpr / total_true_neg
      prec = #ranks / (#ranks + fpr)
      if (#ranks + fpr == 0) then
        prec = 1
      end
      precs[detection_counter] = prec
      if sens[detection_counter]~=sens[detection_counter] and total_true_pos==0 then
        sens[detection_counter] = "ignore me, no edges"
	fprs[detection_counter] = "ignore me, no edges"
        precs[detection_counter] = "ignore me, no edges"
      end
      detection_counter = detection_counter + 1
    end
  end
  return ranks, sens, fprs, precs
end

function find_idx(name, allset)
  ret = -1
  --print('searching for '..name)
  for i=1,#allset do
    --print('  looking at '..allset[i].name)
    if name==allset[i].name then 
      --print('    found')
      ret = i
      break
    end
  end
  return ret
end

function alldists(exist_proj, tofind, use_euclidean) 
  dist = {}
  for i=1,#exist_proj do
    if use_euclidean > 0 then
      d = lpdist(tensor2table(tofind.ex), exist_proj[i].ex, 2) 
      --print(tofind.ex)
    else 
      d = lpdist(tofind.proj, exist_proj[i].proj, 2)
    end
    dist[i] = {d=d, crm=exist_proj[i].crm, idx=i}
  end
  function cmp_dist(x, y)
    return x.d < y.d
  end
  table.sort(dist, cmp_dist)
  return dist
end

function tensor_tostring(t) 
  s = "" .. t[i]
  for i=2,t:size(1) do
    s = s .. "," .. t[i]
  end
  return s
end

function knnlabel(dists, thisproj, thisidx, adjmatrix, k) 
  if k < 1 then
    print("k needs to be larger!")
  end
  nn_idx = {}
  nn_labels = {}
  other_links = adjmatrix[thisidx]
  other_links = tensor2table(other_links)
  
  print("Label is: " .. tensor_tostring(adjmatrix[thisidx]))
  thislabel = 0
  for i=1,k do
    d = dists[i]
    print(d.d, d.crm, d.idx)
    print("NN Label is:" .. tensor_tostring(adjmatrix[d.idx]))
    nn_idx[i] = d.idx
    nn_labels[i] = 0
    if (adjmatrix[d.idx]:sum() > 0) then
      nn_labels[i] = 1
    end
    thislabel = thislabel + nn_labels[i] 
  end
  if thislabel > k/2 then
    return 1
  end
  return 0
end

do_knn = false

all_sens = 0
test_sens = {}
for i=1,#testset do
  projection = mlp:get(1):get(1):forward(testset[i].example)
  print('> ' .. testset[i].name)
  --print("projection:", projection)
  thisproj = {crm=testset[i].comment, proj=tensor2table(projection), ex=testset[i].example}
  dists = alldists(exist_proj, thisproj, use_euclidean)
  print('finished dist', i)
  thisidx = find_idx(testset[i].name, allset)
  --print('this idx', thisidx)
  if not do_knn  then 
    ranks, sens, fprs, precs = getranks(dists, thisproj, thisidx, adjmatrix, randomize)
    if sens~=sens then
      junk = 0
      print("Sens is nan")
    else
      test_sens[i] = sens
      s = table.tostring(sens):gsub("[{}]","")
      print(s)
      if use_fpr then
        s = table.tostring(fprs):gsub("[{}]","")        
        print(s)--, fprs, testset[i].name)
        s = table.tostring(precs):gsub("[{}]","")        
        print(s)--, fprs, testset[i].name)
      end
    end
    --print(all_sens)
    --print(sens)
  else 
    label = knnlabel(dists, thisproj, thisidx, adjmatrix, 3)
    print(label)
  end
end
--print(all_sens/#testset)





