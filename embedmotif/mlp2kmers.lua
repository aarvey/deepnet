require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("../utils/fasta.lua")
dofile("../utils/readcsv.lua")
dofile("../utils/embed.lua")
dofile("../utils/kmer.lua")

function tensor2table(t) 
  tab = {}
  for j=1,t:size(1) do
    tab[j] = t[j]
  end
  return tab
end

-- Compatibility: Lua-5.1
function split(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


function find_idx(name, allfasta)
  ret = -1
  --print('searching for '..name)
  for i=1,#allfasta do
    --print('  looking at '..allfasta[i].name)
    if name==allfasta[i].name then 
      --print('    found')
      ret = i
      break
    end
  end
  return ret
end



----------------------------------
if #arg < 4 then 
  print("Usage: lua mlp2ranks.lua MLPFILE  ALLFASTA  ADJMATRIX CRMS_OF_INTERST")
  os.exit()
end

-----------------------------------

mlp_fname = arg[1]
ext = mlp_fname:sub(mlp_fname:len()-2, mlp_fname:len())
if ext == ".gz" then
  cmd = "gzip -d " .. mlp_fname
  os.execute(cmd)
  mlp_fname = mlp_fname:sub(1,mlp_fname:len()-3)
end
print("Reading embedding mlp from " .. mlp_fname)
f = torch.DiskFile(mlp_fname, "r")
mlp = f:readObject() 
f:close()

kmersize = 6

print("kmer weights:")
for i=1,4^kmersize do
  w = mlp:get(1):get(1):get(1).weight[i]
  --print(idx_to_kmer(i-1,kmersize) .. ":" .. w[1] .. " " .. w[2])
end


--------------------------------------------  

allfastafile = arg[2]
allfasta, motifStrs = motif_fasta_load(allfastafile)
print("Finished reading fasta file")
print("Finished converting fasta file to dataset")
print("Number of sequences " .. #allfasta)
--print("Size of first sequence is:" .. allfasta[1].s:len())


rawdata = {}
for i=1,#allfasta do
  allfasta[i].example = example_to_kmers(allfasta[i].s, kmersize)
  rawdata[i] = allfasta[i].example
  s = allfasta[i].comment
  name = s:sub(1,s:len())
  allfasta[i].name = name
  --print(allfasta[i].name)
  --break
end

exist_proj = {}
for i=1,#allfasta do
  projection = mlp:get(1):get(1):forward(allfasta[i].example)
  exist_proj[#exist_proj+1] = {crm=allfasta[i].comment, proj=tensor2table(projection)}
  --break
end

--------------------------------------------  

adjmatrixfname = arg[3]
print("Reading " .. adjmatrixfname)
adjmatrix, indim = readcsv(adjmatrixfname)
print("Finished reading " .. adjmatrixfname)

--------------------------------------------

ignorecrms = {}
if #arg > 4 then
  ignorecrms_fname = arg[5]
  print("Reading " .. ignorecrms_fname)
  for line in io.lines(ignorecrms_fname) do
    ignorecrms[line] = 1
    print("will be ignoring", line, idx)
  end
  print("Finished Reading " .. ignorecrms_fname)
end

------------------------------------

focuscrms_fname = arg[4]
print("Reading " .. focuscrms_fname)
focuscrms = {}
indices = {}
for line in io.lines(focuscrms_fname) do repeat
  if ignorecrms[line] == 1 then break end -- continue
  idx = find_idx(line, allfasta)
  focuscrms[#focuscrms+1] = {name=line, idx=idx}
  indices[#indices+1] = idx
  print(line, idx)
until true end
print("Finished Reading " .. focuscrms_fname)

--------------------------------------------  

function getcentroid(points) 
  centroid = torch.Tensor(points[1]:size(1))
  for i=1,#points do
    centroid = centroid + points[i]
  end
  centroid = centroid / #points
  return centroid
end

function centroiddist(points, centroid)
  dist = 0
  for i=1,#points do
    dist = dist + lpdist(tensor2table(points[i]*10000), tensor2table(centroid*10000), 2)
  end
  return dist
end

projs = {}
for i=1,#indices do
  projs[i] = torch.Tensor(mlp:get(1):get(1):forward(allfasta[indices[i]].example):size(1))
  projs[i]:copy(mlp:get(1):get(1):forward(allfasta[indices[i]].example))
end
orig_centroid = getcentroid(projs)
print("Original orig_centroid is", orig_centroid)
orig_centroid_dist = centroiddist(projs, orig_centroid)
print("original distance to centroid", orig_centroid_dist)

orig_pair_dist = {}
for i=1,#indices do
  orig_pair_dist[i] = {}
  proj_i = mlp:get(1):get(1):forward(allfasta[indices[i]].example)
  for j=1,#indices do
    proj_j = mlp:get(1):get(1):forward(allfasta[indices[i]].example)
    d = lpdist(tensor2table(proj_i), tensor2table(proj_j), 2)
    orig_pair_dist[i][j] = d
  end
end

function kmer_distortion(kmer, indices, adjmatrix)
  kmeridx = kmer_to_idx(kmer)+1
  distortion = 0

  proj_minus_kmer = {}
  mlp_minus_kmer = mlp:get(1):get(1)
  kmer_weights = torch.Tensor(mlp_minus_kmer:get(1).weight[kmeridx]:size(1))
  kmer_weights:copy(mlp_minus_kmer:get(1).weight[kmeridx])
  --print("kmer weights before zeroing", mlp_minus_kmer:get(1).weight)
  mlp_minus_kmer:get(1).weight[kmeridx]:zero()
  --print("kmer weights after zeroing", mlp_minus_kmer:get(1).weight)
  for i=1,#indices do
    proj_minus_kmer[i] = torch.Tensor(mlp_minus_kmer:forward(allfasta[indices[i]].example):size(1))
    proj_minus_kmer[i]:copy(mlp_minus_kmer:forward(allfasta[indices[i]].example))
  end  
  mlp_minus_kmer:get(1).weight[kmeridx]:copy(kmer_weights)
  --print("kmer weights after restoring", mlp_minus_kmer:get(1).weight)
  --print("orig kmer weights", mlp:get(1):get(1):get(1).weight)
  
  wokmer_pair_dist = {}
  for i=1,#indices do
    wokmer_pair_dist[i] = {}
    proj_i = proj_minus_kmer[i]
    for j=1,#indices do
      proj_j = proj_minus_kmer[j]
      d = lpdist(tensor2table(proj_i), tensor2table(proj_j), 2)
      wokmer_pair_dist[i][j] = d
    end
  end

  c = getcentroid(proj_minus_kmer)
  distortion = centroiddist(proj_minus_kmer, c)
  return distortion
end

function cmp_dist(x, y)
  return x.d < y.d
end

function alldists(exist_proj, tofind) 
  dist = {}
  --print('exist_proj', #exist_proj)
  for i=1,#exist_proj do
    d = lpdist(tofind.proj, exist_proj[i].proj, 2)
    dist[i] = {d=d, crm=exist_proj[i].crm, idx=i}
  end
  table.sort(dist, cmp_dist)
  return dist
end

all_sens = 0
kmersize = 6
kmer_distortions = {}
for i=1,4^kmersize do
  kmer = idx_to_kmer(i-1, kmersize)
  d = kmer_distortion(kmer, indices, adjmatrix)
  kmer_distortions[i] = {kmer=kmer, d=d}
  print(kmer .. ", " .. d)
end

-- table.sort(kmer_distortions, cmd_dist)

-- Challenges
--  * norm of distortion (use centroid distance as norm?)
--  * defining clusters (somewhat arbitrary)
--  * 
--  * 
--  * 
--  * 

