require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

if (#arg < 1) then
   print("embedseq <filename>");
   print("      where <filename> is a dataset with the following format:");
   print("      feature1, feature2, feature3, ..., featureN, label;");
   print("      where all features are numbers");
   return;
end

fname = arg[1]
print("Reading " .. fname)

-- Parse file format (feature vector & label)
-- Each feature is seperated by a comma
-- The label is the final feature and is followed by a semi-colon
points = {}
i = 0
indim = 0
for line in io.lines(fname) do
  i = i + 1
  n = string.len(line)

  -- determine dimension space of points  
  dim = 1
  for k=1,n do
    if string.sub(line,k,k)=="," then dim=dim+1 end
    if string.sub(line,k,k)==";" then break end
  end  
  local point = torch.Tensor(dim)
  indim = dim

  -- get the point's values
  j = 0;  
  k = 0;  
  split_i = 1;
  while (k<=n) do 
    k = k + 1;
    if (string.sub(line,k,k)=="," or string.sub(line,k,k)==";") then
      j = j + 1;
      --print(j, split_i, k-1, n)
      point[j] = tonumber(string.sub(line, split_i, k-1))
      split_i = k+1
    end
  end
  points[i] = point
  --print(line,point)
end
print("Finished reading " .. fname)
print("Input dimension: " .. indim)

-- The output dimension of the embedding
outdim = 2
print("Output dimension: " .. outdim)

-- A table for two mlp's that will share weights and bias
pr_mlp={}

-- Set up the two mlp's 
--   Both will be sequential
--   Both will have a linear representation
--   The input and output spaces of the Linear layer 
for i=1,2 do
  pr_mlp[i]=nn.Sequential()
  pr_mlp[i]:add(nn.Linear(#points,outdim))
end

-- For each layer of pr_mlp1, go through and set the weight & bias to
-- be the same.

for i=1,pr_mlp[1]:size() do
  pr_mlp[2]:get(i).weight:set(pr_mlp[1]:get(i).weight)
  pr_mlp[2]:get(i).bias:set(pr_mlp[1]:get(i).bias)
end

-- Get pairs of examples as input.  The pairs will be points, the
-- target (see below) will be if they are related or not.
prl = nn.ParallelTable()
prl:add(pr_mlp[1])
prl:add(pr_mlp[2])
 
-- Put the mlps into a larger mlp with a pairwise distance to measure
-- how bad we're doing

mlp= nn.Sequential()
mlp:add(prl)
mlp:add(nn.PairwiseDistance(2))

-- Criterion for measuring similarity.
-- Value of +1 means similar
-- Value of -1 means dissimilar

criterion = nn.HingeEmbeddingCriterion(1)

print("Setting up 1-hot encoding")
-- One-hot representation
indices = {}
for i=1,#points do
  indices[i] = torch.Tensor(#points)
  indices[i][i] = 1
  -- print(indices[i])
end


function getidx(idx) 
  for i=1,idx:size(1) do
    if idx[i] == 1 then
      return i
    end
  end
  return -1
end

-- print(3,getidx(indices[3]))
-- print(1,getidx(indices[1]))
-- print(5,getidx(indices[5]))

function gettarget(p1, p2) 
  dist = 0
  if p1:size(1) == p2:size(1) then
    for i=1,p1:size(1) do
      dist = dist + (p1[i] - p2[i])^2
    end
    dist = math.sqrt(dist)
  else 
    print("You need to have same size!")  
    error("You need to have same size!")  
  end

  --print(string.format("%0.3f",dist), p1[1]..","..p1[2], p2[1]..","..p2[2])

  if dist < 2 then
    return 1
  else 
    return -1
  end
end

print("Starting to set up examples")
examples = {}
k = 1
subsample = 10
posdup = 1
negdup = 1
print("Number of points: " .. #points)
for i=1,#points,subsample do
  for j=1,#points,subsample do repeat
    if j==i then break end -- Acts as continue, don't need to specify reflexive
    target = gettarget(points[i], points[j])
    if target < 0 then break end

    examples[k] = {}
    examples[k][1] = {indices[i], indices[j]}
    --print(string.format("F( (%2d,%2d) , (%2d,%2d) ) = %d", points[i][1],points[i][2], points[j][1],points[j][2], target))
    examples[k][2] = target
    k = k + 1
  until true end
  if i % 21 == 0 then 
    print("Done with point " .. i)
  end
end
print("Examples are now set up")

posexamples = {}
negexamples = {}
numneg = 0
for i=1,#examples do 
  if examples[i][2] <= 0 then
    negexamples[i] = examples[i]
    numneg = numneg + 1
  else
    posexamples[i] = examples[i]    
  end
end
numpos = #examples - numneg

negdup = math.ceil(numpos/numneg)
posdup = 1
if negdup == 1 then
  posdup = math.floor(numneg/numpos)
end


posdup=1
negdup=1
winsize = 400
dofile("../utils/embed.lua")
trainembedding(mlp, criterion, indices, nil, posexamples, negexamples, posdup, negdup)