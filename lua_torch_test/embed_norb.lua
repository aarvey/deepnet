require("math")
dofile("draw.lua")
dofile("../utils/readfile.lua")
dofile("../utils/embed.lua")


function shuffle(table)
  local shuffledIdx = lab.randperm(#table)
  newtable = {}
  for i=1,#table do
    newtable[i] = table[shuffledIdx[i]]
  end
  return newtable
end


-----------------------------------
if (#arg < 2) then
  print("embedmatrix <pair-filename> <raw-filename>");
  print("")
  print("      Where <pair-filename> is a list of pairs that are similar")
  print("      with the following format:")
  print("      idx1, idx2;")
  print("      Any nonspecified pairs are assumed to be negative examples")
  print("      Total number of items is infered from the maximum index")
  print("")
  print("      If specified the <raw-filename> is data in vector format")
  print("      x1, x2, x3, ... xn;")
  print("      Where x may be an image, feature space, or any other data")
  return
end
-----------------------------------

-----------------------------------
fname = arg[1]
print("Reading " .. fname)
pairs, indim = readfile(fname)
print("Finished reading " .. fname)
print("Input dimension: " .. indim)


numpoints = 99
--numpoints = 659
print("Number of points: " .. numpoints)

local s = ""
k = 1
simtable = {}
for i=1,#pairs do repeat 
  if pairs[i][1] + 1 > numpoints or pairs[i][2] + 1 > numpoints then
    break
  end
  simtable[k] = {pairs[i][1] + 1, pairs[i][2] + 1}
  k = k + 1
until true end
-----------------------------------

-----------------------------------
rawfname = arg[2]
print("Reading " .. rawfname)
rawdata, indim = readfile(rawfname, numpoints)
print("Finished reading " .. rawfname)
print("Raw Input dimension: " .. indim)
-----------------------------------

for i=1,#rawdata do
  for j=1,indim do
    rawdata[i][j] = rawdata[i][j] / 255.0
  end
end


print("Rawdata type: " .. type(rawdata[1][1]))
print("Rawdata type: " .. type(rawdata[1]))
print("Rawdata type: " .. type(rawdata))
print("Rawdata 1: " .. rawdata[1][1])
print("Rawdata 2: " .. rawdata[2][1])
print("Rawdata 3: " .. rawdata[3][1])

radius = 15
winsize = 500

circles = {}
numcircles = numpoints
numpoints = numcircles
for i=1,numcircles do
  circles[i] = {}
  circles[i].x = math.random()*winsize
  circles[i].y = math.random()*winsize
end

outdim = 3
mlp = getembedmlp(indim, outdim)
simmatrix = getsimmatrix(simtable, numpoints)
print(simmatrix[1][2], simmatrix[3][11], simmatrix[3][15])
print(simmatrix[1][4])
posexamples = getposexamples(simtable, numpoints, rawdata)
--print(posexamples[3][1][1],posexamples[3][1][2],posexamples[3][2])
negexamples = getnegexamples(simmatrix, numpoints, rawdata)

--print("Bias is: ", mlp:get(1):get(1):get(2).bias)

w = lcairo.Window(winsize,winsize)
cr = lcairo.Cairo(w)
--w_weights = lcairo.Window(winsize,winsize)
--cr_weights = lcairo.Cairo(w)

print("Number of examples: " .. #posexamples+#negexamples)
print("Number of positive examples: " .. #posexamples)
print("Number of negative examples: " .. #negexamples)

local lr = 0.0003
local lrdecay = 0.005
local lrmax = 0.0003
local lrmin = 0.00005
criterion = nn.HingeEmbeddingCriterion(2)
numepochs=2000
for j=1,numepochs do
  posidx = 1
  negidx = 1
  posexamples = shuffle(posexamples)
  negexamples = shuffle(negexamples)
  --print("Bias is: ", mlp:get(1):get(1):get(2).bias)
  while true do 
    if math.random(1,#posexamples+#negexamples) < #posexamples or negidx > #negexamples then
      if posidx <= #posexamples then 
        gradUpdate(mlp, posexamples[posidx][1], posexamples[posidx][2], criterion, lr)
        --layer1bias = mlp:get(1):get(1):get(2).bias
        --layer1bias = mlp:get(1):get(1):get(2).weight
        --print("Positive grad update bias is: ", layer1bias)
        --if (math.abs(layer1bias[1]) < 0.0001 or layer1bias[1]~=layer1bias[1]) and posidx < 10 then
        --print("Positive example: " .. posidx, posexamples[posidx][1][1], posexamples[posidx][1][2], posexamples[posidx][2])
        --end
      end
      posidx = posidx + 1
    else 
      if negidx <= #negexamples then 
        gradUpdate(mlp, negexamples[negidx][1], negexamples[negidx][2], criterion, lr)
        --layer1bias = mlp:get(1):get(1):get(2).bias
        --layer1bias = mlp:get(1):get(1):get(2).weight
        --print("Negative grad update bias is: ", layer1bias)
      end 
      negidx = negidx + 1
    end      

    if negidx % 10000 == 0 then
      print("Learning gradient from point " .. posidx, negidx)
    end

    if negidx > #negexamples and posidx > #posexamples then break end
  end

  lr = lr / (1 + lrdecay)
  if lr > lrmax or lr < lrmin then
    lrdecay = - lrdecay
  end

  print("epochs="..j..", lr="..lr)
  
  maxx = -999
  minx = 999
  maxy = -999
  miny = 999

  for k=1,numpoints do
    embedding_layer = getembedlayer(mlp)
    --print("Forward bias is: ", mlp:get(1):get(1):get(2).bias)
    embed = embedding_layer:forward(rawdata[k])
    --print(embed:size())
    x = embed[1]
    y = embed[2]
    circles[k].x = x
    circles[k].y = y
    if x > maxx then maxx = x end
    if y > maxy then maxy = y end
    if y < miny then miny = y end
    if x < minx then minx = x end
    --print("Circle (x,y)=("..x..","..y..")")
  end

  -- Normalize x, y of circle
  if miny > 0 then miny = 0 end
  if minx > 0 then minx = 0 end
  for k=1,numpoints do
    --print("before x,y", circles[k].x, circles[k].y)
    circles[k].x = ((circles[k].x + -minx) / (maxx-minx)) * winsize
    circles[k].y = ((circles[k].y + -miny) / (maxy-miny)) * winsize
    --print("after x,y", circles[k].x, circles[k].y)
  end
  clear(w,cr)
  drawgraph(w,cr,circles,simtable)
  --cr:sleep(1)
end






function printfunctions()
  for k, v in pairs(lcairo) do
    print(k, "->", v)
  end
end
--printfunctions()





