require("gfx")
require("lcairo")
require("math")

function drawcircle(cr, x,y, rad, label)
  cr:arc(x,y,rad,0,2*math.pi)
  cr:stroke()
  if label ~= "" then
    print("Label is", label)
    
  end
end

function drawline(cr, x1,y1, x2,y2)
  cr:set_line_width(2.0)
  cr:move_to(x1,y1)
  cr:line_to(x2,y2)
  cr:stroke()
end

function clear(w,cr)
  cr:set_source_rgb(1,1,1)
  cr:paint()
  w:refresh()
end

function drawscreen(w,cr,circles,simmatrix)
  cr:set_source_rgb(0,0,0)
  for i=1,#circles do
    x = circles[i].x
    y = circles[i].y
    drawcircle(cr, x, y, radius, "")
  end
  for i=1,#simmatrix do
    start = simmatrix[i][1]
    fin = simmatrix[i][2]
    drawline(cr, circles[start].x,circles[start].y,  circles[fin].x,circles[fin].y)
  end
  w:refresh()
end
