require("torch");
require("nn");
require("os");

k = 7
d = 8 -- (2*k)-d represents the amount of compression we seek in the embedding

module = nn.LookupTable(10, 3) 
input = torch.Tensor(4)
input[1] = 1; input[2] = 2; input[3] = 1; input[4] = 10;
print(module:forward(input))
input[1] = 1; input[2] = 2; input[3] = 3; input[4] = 6;
print(module:forward(input))
input[1] = 3; input[2] = 2; input[3] = 1; input[4] = 1;
print(module:forward(input))



module = nn.LookupTable(4^k, d) 


os.exit()

function getsimseqs()
  
end


function getdiffseqs()
  
end



-- The data file (data points & labels), format described below
if (#arg < 1) then
   print("embedseq <filename>");
   print("      where <filename> is a dataset with the following format:");
   print("      feature1, feature2, feature3, ..., featureN, label;");
   print("      where all features are numbers");
   return;
end

fname = arg[1]

-- Set up an array to house all data points & labels
dataset = {}
function dataset:size() return #dataset end
print(dataset:size())
i = 0;
for line in io.lines(fname) do
  i = i + 1;
  local point = torch.Tensor(2);
  local label = torch.Tensor(1);
  
  -- Parse file format (feature vector & label)
  -- Each feature is seperated by a comma
  -- The label is the final feature and is followed by a semi-colon
  n = string.len(line);
  j = 0;  k = 0;  split_i = 1;
  while (k<=n) do 
    k = k + 1;
    if (string.sub(line,k,k)==",") then
      j = j + 1;
      point[j] = tonumber(string.sub(line, split_i, k-1));
      split_i = k+1;
    end
    if (string.sub(line,k,k)==";") then
      label[1] = string.sub(line, split_i, k-1);
    end
  end

  -- Add data point (features) and label to the example  
  example = {point, label};
  dataset[i] = example;

  -- Print out progress
  if (math.fmod(i,1000)==0) then
    print("Read in " .. i .. " points");
  end

end


-- Get a NN regression classifier from a dataset
-- It is also possible to make slight modifications to enable 
-- multiclass and multilabel prediction problems.
function getNN(data)
  function data:size() return #data end
  mlp = nn.Sequential();
  mlp:add( nn.Linear(2, 15) );
  mlp:add( nn.HardTanh() );
  mlp:add( nn.Linear(15, 1) );

  --format=ml.DatasetClassFormat(2)
  --criterion = nn.ClassNLLCriterion(format);
  criterion = nn.MSECriterion();
  trainer = nn.StochasticGradient(mlp, criterion);
  trainer.learningRate = 0.01;
  trainer:train(data);
  
  return mlp;
end

-- Get the error of a NN classifier on a data set
function getErr(mlp, data)
  err = 0;
  for i=1,#data do
    local pred = mlp:forward(data[i][1]);
    local e = 1;
    if ((pred[1] > 0.5 and data[i][2][1] > 0.5) or
        (pred[1] < 0.5 and data[i][2][1] < 0.5)) then
       e = 0
    end
    err = err + e
  end
  return err / #data;
end

-- Take in a dataset and randomly shuffle the items in the set
function shuffle(data)
  newdata = {}
  for i=1,#data do
    local rand_idx = math.random(1,#data);
    local ex = data[rand_idx];
    newdata[#newdata+1] = ex;
    table.remove(data, rand_idx);
  end
  return newdata;
end

-- Do k-fold cross validation on the dataset
function runCV(data, numFolds)
  data = shuffle(data)
  avg_err = 0
  for i=1,numFolds do
    local test = {};
    local train = {};
    for j=1,#data do
      if (j > (#data*(i-1)/numFolds) and
          j <= (#data*(i)/numFolds)) then
        test[#test+1] = data[j];
      else
        train[#train+1] = data[j];
      end
    end
    mlp = getNN(train);
    err = getErr(mlp,test);
    avg_err = avg_err + err
    print("FOLD " .. i .. " ERR: " .. err);
  end
  print("AVERAGE ERROR: " .. avg_err/numFolds);
end


runCV(dataset, 5)

