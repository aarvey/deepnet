require("gfx")
require("lcairo")
require("math")
dofile("../utils/readfile.lua")
dofile("../utils/embed.lua")

function plot(t)
  w = lcairo.Window(200,200)
  cr = lcairo.Cairo(w)
  cr:set_line_width(2.0)
  cr:move_to(80,100)
  cr:line_to(100,120)
  cr:line_to(160,20)
  cr:stroke()
  w:refresh()
end

function drawcircle(cr, x,y, rad, label)
  cr:arc(x,y,rad,0,2*math.pi)
  cr:stroke()
  if label ~= "" then
    print("Label is", label)
    
  end
end

function drawline(cr, x1,y1, x2,y2)
  cr:set_line_width(2.0)
  cr:move_to(x1,y1)
  cr:line_to(x2,y2)
  cr:stroke()
end

function clear(w,cr)
  cr:set_source_rgb(1,1,1)
  cr:paint()
  w:refresh()
end

function drawscreen(w,cr,circles,simmatrix)
  cr:set_source_rgb(0,0,0)
  for i=1,#circles do
    x = circles[i].x
    y = circles[i].y
    drawcircle(cr, x, y, radius, "")
  end
  for i=1,#simmatrix do
    start = simmatrix[i][1]
    fin = simmatrix[i][2]
    drawline(cr, circles[start].x,circles[start].y,  circles[fin].x,circles[fin].y)
  end
  w:refresh()
end

function shuffle(table)
  local shuffledIdx = lab.randperm(#table)
  newtable = {}
  for i=1,#table do
    newtable[i] = table[shuffledIdx[i]]
  end
  return newtable
end


radius = 15
winsize = 500

circles = {}
simtable = {}
numcircles = 20
numpoints = numcircles
for i=1,numcircles do
  circles[i] = {}
  circles[i].x = math.random()*winsize
  circles[i].y = math.random()*winsize
end

k = 1
for i=3,23 do
  for j=i-2,i do
    if i > numpoints then i = i - 20 end
    if j > numpoints then j = j - 20 end
    simtable[k] = {i,j}
    k = k + 1
  end
end

--[[
simtable[k] = {19,20}; k = k + 1;
simtable[k] = {18,19}; k = k + 1;
simtable[k] = {18,20}; k = k + 1;
simtable[k] = {1,20}; k = k + 1;
simtable[k] = {1,19}; k = k + 1;
simtable[k] = {2,20}; k = k + 1;
--]]

--[[
for i=1,5 do
  circles[i] = {}
end
circles[1].x = 50
circles[2].x = 100
circles[3].x = 100
circles[4].x = 150
circles[5].x = 150
circles[1].y = 100
circles[2].y = 50
circles[3].y = 150
circles[4].y = 100
circles[5].y = 50

simtable[1] = {1,2}
simtable[2] = {3,2}
simtable[3] = {1,4}
simtable[4] = {3,4}
simtable[5] = {2,5}
--]]

indices = encode1hot(numpoints)
outdim = 2
mlp = getembedmlp(numcircles, outdim)
simmatrix = getsimmatrix(simtable, numpoints)
posexamples = getposexamples(simtable, numpoints, indices)
negexamples = getnegexamples(simmatrix, numpoints, indices)




w = lcairo.Window(winsize,winsize)
cr = lcairo.Cairo(w)
--[[
drawscreen(w,cr,circles,simtable)
cr:sleep(2)
clear(w,cr)
cr:sleep(2)
drawscreen(w,cr,circles,simtable)
cr:sleep(2)
clear(w,cr)
print("Just painted screen")
--]]


print("Number of examples: " .. #posexamples+#negexamples)
print("Number of positive examples: " .. #posexamples)
print("Number of negative examples: " .. #negexamples)

local lr = 0.0025
local lrdecay = 0 -- -0.008
local lrmax = 0.0023
local lrmin = 0.0005
criterion = nn.HingeEmbeddingCriterion(0.8)
numepochs=2000
for j=1,numepochs do
  posidx = 1
  negidx = 1
  posexamples = shuffle(posexamples)
  negexamples = shuffle(negexamples)
  while true do 
    if math.random(1,#posexamples+#negexamples) < #posexamples or negidx > #negexamples then
      if posidx <= #posexamples then 
        gradUpdate(mlp, posexamples[posidx][1], posexamples[posidx][2], criterion, lr)
      end
      posidx = posidx + 1
    else 
      if negidx <= #negexamples then 
        gradUpdate(mlp, negexamples[negidx][1], negexamples[negidx][2], criterion, lr)
      end 
      negidx = negidx + 1
    end      

    if negidx % 10000 == 0 then
      print("Learning gradient from point " .. posidx, negidx)
    end

    if negidx > #negexamples and posidx > #posexamples then break end
  end

  lr = lr / (1 + lrdecay)
  if lr > lrmax or lr < lrmin then
    lrdecay = - lrdecay
  end

  print("epochs="..j..", lr="..lr)
  
  maxx = -999
  minx = 999
  maxy = -999
  miny = 999

  for k=1,numpoints do
    embed = mlp:get(1):get(1):forward(indices[k])
    --print(embed:size())
    x = embed[1]
    y = embed[2]
    circles[k].x = x
    circles[k].y = y
    if x > maxx then maxx = x end
    if y > maxy then maxy = y end
    if y < miny then miny = y end
    if x < minx then minx = x end
  end

  -- Normalize x, y of circle
  if miny > 0 then miny = 0 end
  if minx > 0 then minx = 0 end
  for k=1,numpoints do
    --print("before x,y", circles[k].x, circles[k].y)
    circles[k].x = ((circles[k].x + -minx) / (maxx-minx)) * winsize
    circles[k].y = ((circles[k].y + -miny) / (maxy-miny)) * winsize
    --print("after x,y", circles[k].x, circles[k].y)
  end
  drawscreen(w,cr,circles,simtable)
  --cr:sleep(1)
  clear(w,cr)
end




--rawfname = arg[1]
--print("Reading " .. rawfname)
--rawdata, indim = readfile(rawfname)
--print("Finished reading " .. rawfname)
--print("Raw Input dimension: " .. indim)



function printfunctions()
  for k, v in pairs(lcairo) do
    print(k, "->", v)
  end
end
--printfunctions()





