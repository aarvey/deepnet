require("torch");
require("nn");
require("os");
require("lab");
require("math");
require("random");
require("io");

dofile("../utils/readcsv.lua")
dofile("../utils/embed.lua")

-----------------------------------
if (#arg < 2) then
  print("embedmatrix <pair-filename> <raw-filename>");
  print("")
  print("      Where <pair-filename> is a list of pairs that are similar")
  print("      with the following format:")
  print("      idx1, idx2;")
  print("      Any nonspecified pairs are assumed to be negative examples")
  print("      Total number of items is infered from the maximum index")
  print("")
  print("      If specified the <raw-filename> is data in vector format")
  print("      x1, x2, x3, ... xn;")
  print("      Where x may be an image, feature space, or any other data")
  return
end
-----------------------------------

-----------------------------------
fname = arg[1]
print("Reading " .. fname)
simtable, indim = readfile(fname)
print("Finished reading " .. fname)
print("Input dimension: " .. indim)

maxpoint = -9
local s = ""
for i=1,#simtable do
  s = ""
  for j=1,indim do 
    simtable[i][j] = simtable[i][j] + 1 
    if simtable[i][j] > maxpoint then
      maxpoint = simtable[i][j]
    end
    --s = s .. " " .. simtable[i][j]
  end
  --print(i,s)
end

numpoints = maxpoint
print("Number of points: " .. numpoints)
-----------------------------------

-----------------------------------
rawfname = arg[2]
print("Reading " .. rawfname)
rawdata, indim = readfile(rawfname)
print("Finished reading " .. rawfname)
print("Raw Input dimension: " .. indim)

outdim = 3
print("Output dimension: " .. outdim)
-----------------------------------

-----------------------------------
maxpoint = numpoints
maxpoint = 99
-----------------------------------


-- Criterion for measuring similarity.
-- Value of +1 means similar
-- Value of -1 means dissimilar
criterion = nn.HingeEmbeddingCriterion(1)

print("Setting up 1-hot encoding")
-- One-hot representation
indices = {}
for i=1,numpoints do
  if i > maxpoint then break end
  indices[i] = torch.Tensor(numpoints)
  indices[i][i] = 1
end

function getidx(idx) 
  for i=1,idx:size(1) do
    if idx[i] > 0 then
      return i
    end
  end
  return -1
end

--print(3,getidx(indices[3]))
--print(1,getidx(indices[1]))
--print(5,getidx(indices[5]))

print("Starting to set up examples")
local simmatrix = {}
local i = 0
local j = 0
for k=1,#simtable do repeat
  i = simtable[k][1]
  j = simtable[k][2]
  if i > maxpoint or j > maxpoint then break end
  if simmatrix[i] == nil then
    simmatrix[i] = {}
  end
  if simmatrix[j] == nil then
    simmatrix[j] = {}
  end
  simmatrix[i][j] = 1
  simmatrix[j][i] = 1
  simmatrix[i][i] = 1
  simmatrix[j][j] = 1
until true end
for i=1,numpoints do repeat
  if i > maxpoint then break end
  print(i)
  simmatrix[i][i] = 1
until true end


posexamples = {}
local k = 1
for l=1,#simtable do repeat
  i = simtable[l][1]
  j = simtable[l][2]
  if i > maxpoint or j > maxpoint then break end
  posexamples[k] = {}
  posexamples[k][1] = {rawdata[i], rawdata[j]}
  posexamples[k][2] = 1
  k = k + 1
until true end

k = 1
negexamples = {}
for i=1,numpoints do
  for j=i+1,numpoints do repeat 
    if i > maxpoint or j > maxpoint then break end
    if simmatrix[i][j]==nil then 
      negexamples[k] = {}
      negexamples[k][1] = {rawdata[i], rawdata[j]}
      negexamples[k][2] = -1
      --print('neg',i,j)
      k = k +1
    end
  until true end
end

print("Examples are now set up")
print("Number of positive examples: " .. #posexamples)
print("Number of negative examples: " .. #negexamples)


posdup=1
negdup=1
mlp = getembedmlp(numpoints, rawdata, indim, outdim)
trainembedding(mlp, criterion, indices, rawdata, posexamples, negexamples, posdup, negdup)


