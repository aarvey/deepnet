require("torch")
require("lab")
require("nn")


-- Question: Does this set the pointers to be the same???
-- Answer: No.  We just have to update the parameters at the same time.

x = lab.rand(2)

mlp1 = nn.Sequential()
mlp1:add(nn.Linear(2,2))
print(mlp1:forward(x))

mlp2 = nn.Sequential()
mlp2:add(nn.Linear(2,2))
print(mlp2:forward(x))

mlp2:get(1).weight:set(mlp1:get(1).weight)
mlp2:get(1).bias:set(mlp1:get(1).bias)
print(mlp2:forward(x))
print(mlp1:forward(x))

mlp2:get(1).weight:set(lab.zeros(2,2))
mlp2:get(1).bias:set(lab.zeros(2))
print(mlp2:forward(x))
print(mlp1:forward(x))




